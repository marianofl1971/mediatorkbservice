% Automatically generated from transformer.jar (see javadoc in transformer/html)

:- discontiguous(has_adduct/4).
:- discontiguous(is_directly_defined_instance_of/4).
:- discontiguous(ionization/2).
:- discontiguous(modifier/2).

modifier(none, sure).

ionization(1, sure).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_32675, hypothesised, hypothesis(3)).

has_adduct(e_compound(399.3367, 18.8425), 'M+H', hypothesised, hypothesis(3)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_32694, hypothesised, hypothesis(4)).

has_adduct(e_compound(399.3367, 18.8425), 'M+H', hypothesised, hypothesis(4)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_32600, hypothesised, hypothesis(5)).

has_adduct(e_compound(399.3367, 18.8425), 'M+H', hypothesised, hypothesis(5)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_127498, hypothesised, hypothesis(6)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(6)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_117665, hypothesised, hypothesis(7)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(7)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_135390, hypothesised, hypothesis(8)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(8)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_84238, hypothesised, hypothesis(9)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(9)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_147233, hypothesised, hypothesis(10)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(10)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_80166, hypothesised, hypothesis(11)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(11)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_88361, hypothesised, hypothesis(12)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(12)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_146734, hypothesised, hypothesis(13)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(13)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_132916, hypothesised, hypothesis(14)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(14)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_104502, hypothesised, hypothesis(15)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(15)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_70206, hypothesised, hypothesis(16)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(16)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_51011, hypothesised, hypothesis(17)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(17)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_123216, hypothesised, hypothesis(18)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(18)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_53077, hypothesised, hypothesis(19)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(19)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_62551, hypothesised, hypothesis(20)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(20)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_77415, hypothesised, hypothesis(21)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(21)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_71795, hypothesised, hypothesis(22)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(22)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_140682, hypothesised, hypothesis(23)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(23)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_137614, hypothesised, hypothesis(24)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(24)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_89257, hypothesised, hypothesis(25)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(25)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_123307, hypothesised, hypothesis(26)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(26)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_129455, hypothesised, hypothesis(27)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(27)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_74160, hypothesised, hypothesis(28)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(28)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_138673, hypothesised, hypothesis(29)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(29)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_129206, hypothesised, hypothesis(30)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(30)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_121275, hypothesised, hypothesis(31)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(31)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_105410, hypothesised, hypothesis(32)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(32)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_44750, hypothesised, hypothesis(33)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(33)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_94928, hypothesised, hypothesis(34)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(34)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_95442, hypothesised, hypothesis(35)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(35)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_91373, hypothesised, hypothesis(36)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(36)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_65787, hypothesised, hypothesis(37)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(37)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22542, hypothesised, hypothesis(38)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(38)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22543, hypothesised, hypothesis(39)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(39)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21008, hypothesised, hypothesis(40)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(40)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21524, hypothesised, hypothesis(41)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(41)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22038, hypothesised, hypothesis(42)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(42)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21552, hypothesised, hypothesis(43)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(43)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22064, hypothesised, hypothesis(44)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(44)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21062, hypothesised, hypothesis(45)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(45)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21079, hypothesised, hypothesis(46)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(46)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22133, hypothesised, hypothesis(47)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(47)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_23673, hypothesised, hypothesis(48)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(48)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21626, hypothesised, hypothesis(49)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(49)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21119, hypothesised, hypothesis(50)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(50)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22154, hypothesised, hypothesis(51)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(51)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22155, hypothesised, hypothesis(52)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(52)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22923, hypothesised, hypothesis(53)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(53)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22924, hypothesised, hypothesis(54)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(54)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21650, hypothesised, hypothesis(55)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(55)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22175, hypothesised, hypothesis(56)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(56)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22947, hypothesised, hypothesis(57)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(57)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22444, hypothesised, hypothesis(58)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(58)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21171, hypothesised, hypothesis(59)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(59)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22991, hypothesised, hypothesis(60)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(60)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_20960, hypothesised, hypothesis(61)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(61)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21734, hypothesised, hypothesis(62)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(62)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21223, hypothesised, hypothesis(63)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(63)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_23275, hypothesised, hypothesis(64)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(64)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_23276, hypothesised, hypothesis(65)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(65)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_22519, hypothesised, hypothesis(66)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(66)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21752, hypothesised, hypothesis(67)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(67)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_21753, hypothesised, hypothesis(68)).

has_adduct(e_compound(399.3367, 18.8425), 'M+2H', hypothesised, hypothesis(68)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_26660, hypothesised, hypothesis(71)).

has_adduct(e_compound(399.3367, 18.8425), 'M+NH4', hypothesised, hypothesis(71)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_26661, hypothesised, hypothesis(72)).

has_adduct(e_compound(399.3367, 18.8425), 'M+NH4', hypothesised, hypothesis(72)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_61005, hypothesised, hypothesis(73)).

has_adduct(e_compound(399.3367, 18.8425), 'M+NH4', hypothesised, hypothesis(73)).

is_directly_defined_instance_of(e_compound(399.3367, 18.8425), t_compound_62845, hypothesised, hypothesis(74)).

has_adduct(e_compound(399.3367, 18.8425), 'M+NH4', hypothesised, hypothesis(74)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_31987, hypothesised, hypothesis(79)).

has_adduct(e_compound(421.3169, 18.8425), 'M+H', hypothesised, hypothesis(79)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_6692, hypothesised, hypothesis(81)).

has_adduct(e_compound(421.3169, 18.8425), 'M+2H', hypothesised, hypothesis(81)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_32675, hypothesised, hypothesis(82)).

has_adduct(e_compound(421.3169, 18.8425), 'M+Na', hypothesised, hypothesis(82)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_32694, hypothesised, hypothesis(83)).

has_adduct(e_compound(421.3169, 18.8425), 'M+Na', hypothesised, hypothesis(83)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_32600, hypothesised, hypothesis(84)).

has_adduct(e_compound(421.3169, 18.8425), 'M+Na', hypothesised, hypothesis(84)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_39703, hypothesised, hypothesis(86)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(86)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_39704, hypothesised, hypothesis(87)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(87)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_41059, hypothesised, hypothesis(88)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(88)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_57357, hypothesised, hypothesis(91)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(91)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_142880, hypothesised, hypothesis(92)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(92)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_118411, hypothesised, hypothesis(93)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(93)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_67510, hypothesised, hypothesis(94)).

has_adduct(e_compound(421.3169, 18.8425), 'M+NH4', hypothesised, hypothesis(94)).

is_directly_defined_instance_of(e_compound(421.3169, 18.8425), t_compound_32638, hypothesised, hypothesis(95)).

has_adduct(e_compound(421.3169, 18.8425), 'M+H-H2O', hypothesised, hypothesis(95)).

is_directly_defined_instance_of(e_compound(315.2424, 8.1449), t_compound_32655, hypothesised, hypothesis(96)).

has_adduct(e_compound(315.2424, 8.1449), 'M+H', hypothesised, hypothesis(96)).

is_directly_defined_instance_of(e_compound(315.2424, 8.1449), t_compound_32602, hypothesised, hypothesis(98)).

has_adduct(e_compound(315.2424, 8.1449), 'M+H', hypothesised, hypothesis(98)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26629, hypothesised, hypothesis(104)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(104)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26632, hypothesised, hypothesis(105)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(105)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27947, hypothesised, hypothesis(106)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(106)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27971, hypothesised, hypothesis(107)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(107)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25682, hypothesised, hypothesis(108)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(108)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27493, hypothesised, hypothesis(109)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(109)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26586, hypothesised, hypothesis(170)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(170)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26610, hypothesised, hypothesis(171)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(171)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26623, hypothesised, hypothesis(172)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(172)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_180668, hypothesised, hypothesis(173)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(173)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27954, hypothesised, hypothesis(174)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(174)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26223, hypothesised, hypothesis(176)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(176)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25971, hypothesised, hypothesis(177)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(177)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25798, hypothesised, hypothesis(178)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(178)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25799, hypothesised, hypothesis(179)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(179)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25800, hypothesised, hypothesis(180)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(180)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25803, hypothesised, hypothesis(181)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(181)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25997, hypothesised, hypothesis(184)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(184)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25804, hypothesised, hypothesis(185)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(185)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26112, hypothesised, hypothesis(186)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(186)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31499, hypothesised, hypothesis(187)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(187)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31500, hypothesised, hypothesis(188)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(188)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31506, hypothesised, hypothesis(189)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(189)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31507, hypothesised, hypothesis(190)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(190)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31508, hypothesised, hypothesis(191)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(191)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31509, hypothesised, hypothesis(192)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(192)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31510, hypothesised, hypothesis(193)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(193)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31771, hypothesised, hypothesis(194)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(194)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26396, hypothesised, hypothesis(195)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(195)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31520, hypothesised, hypothesis(196)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(196)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31522, hypothesised, hypothesis(197)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(197)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31523, hypothesised, hypothesis(198)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(198)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31779, hypothesised, hypothesis(199)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(199)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31524, hypothesised, hypothesis(200)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(200)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25637, hypothesised, hypothesis(201)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(201)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31525, hypothesised, hypothesis(202)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(202)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31526, hypothesised, hypothesis(203)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(203)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31527, hypothesised, hypothesis(204)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(204)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31528, hypothesised, hypothesis(205)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(205)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25645, hypothesised, hypothesis(206)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(206)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27441, hypothesised, hypothesis(207)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(207)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_27456, hypothesised, hypothesis(208)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(208)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25962, hypothesised, hypothesis(209)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(209)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25963, hypothesised, hypothesis(210)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(210)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25964, hypothesised, hypothesis(211)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(211)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26220, hypothesised, hypothesis(212)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(212)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25965, hypothesised, hypothesis(213)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(213)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26221, hypothesised, hypothesis(214)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(214)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25966, hypothesised, hypothesis(215)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(215)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26222, hypothesised, hypothesis(216)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(216)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25967, hypothesised, hypothesis(217)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(217)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25968, hypothesised, hypothesis(218)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(218)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26224, hypothesised, hypothesis(219)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(219)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25969, hypothesised, hypothesis(220)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(220)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26225, hypothesised, hypothesis(221)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(221)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25970, hypothesised, hypothesis(222)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(222)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26226, hypothesised, hypothesis(223)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(223)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26227, hypothesised, hypothesis(224)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(224)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25972, hypothesised, hypothesis(225)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(225)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26228, hypothesised, hypothesis(226)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(226)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25973, hypothesised, hypothesis(227)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(227)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26229, hypothesised, hypothesis(228)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(228)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25974, hypothesised, hypothesis(229)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(229)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26230, hypothesised, hypothesis(230)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(230)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25975, hypothesised, hypothesis(231)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(231)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26231, hypothesised, hypothesis(232)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(232)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25976, hypothesised, hypothesis(233)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(233)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26232, hypothesised, hypothesis(234)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(234)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25977, hypothesised, hypothesis(235)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(235)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25978, hypothesised, hypothesis(236)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(236)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25979, hypothesised, hypothesis(237)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(237)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25980, hypothesised, hypothesis(238)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(238)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25981, hypothesised, hypothesis(239)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(239)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25982, hypothesised, hypothesis(240)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(240)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25983, hypothesised, hypothesis(241)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(241)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25984, hypothesised, hypothesis(242)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(242)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25985, hypothesised, hypothesis(243)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(243)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26497, hypothesised, hypothesis(244)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(244)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25986, hypothesised, hypothesis(245)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(245)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25987, hypothesised, hypothesis(246)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(246)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25988, hypothesised, hypothesis(247)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(247)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25989, hypothesised, hypothesis(248)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(248)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25990, hypothesised, hypothesis(249)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(249)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25991, hypothesised, hypothesis(250)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(250)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25992, hypothesised, hypothesis(251)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(251)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25993, hypothesised, hypothesis(252)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(252)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25994, hypothesised, hypothesis(253)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(253)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25995, hypothesised, hypothesis(254)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(254)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25996, hypothesised, hypothesis(255)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(255)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25998, hypothesised, hypothesis(256)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(256)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25999, hypothesised, hypothesis(257)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(257)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26000, hypothesised, hypothesis(258)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(258)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26001, hypothesised, hypothesis(259)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(259)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26002, hypothesised, hypothesis(260)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(260)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26003, hypothesised, hypothesis(261)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(261)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25497, hypothesised, hypothesis(262)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(262)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25498, hypothesised, hypothesis(263)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(263)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25790, hypothesised, hypothesis(264)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(264)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25791, hypothesised, hypothesis(265)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(265)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25792, hypothesised, hypothesis(266)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(266)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25793, hypothesised, hypothesis(267)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(267)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25794, hypothesised, hypothesis(268)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(268)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25795, hypothesised, hypothesis(269)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(269)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25796, hypothesised, hypothesis(270)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(270)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25797, hypothesised, hypothesis(271)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(271)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25801, hypothesised, hypothesis(272)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(272)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25802, hypothesised, hypothesis(273)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(273)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25805, hypothesised, hypothesis(274)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(274)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25806, hypothesised, hypothesis(275)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(275)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_25807, hypothesised, hypothesis(276)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(276)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_31715, hypothesised, hypothesis(277)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(277)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_32246, hypothesised, hypothesis(278)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(278)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26360, hypothesised, hypothesis(279)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(279)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_32253, hypothesised, hypothesis(280)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(280)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26110, hypothesised, hypothesis(281)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(281)).

is_directly_defined_instance_of(e_compound(280.2402, 28.2695), t_compound_26111, hypothesised, hypothesis(282)).

has_adduct(e_compound(280.2402, 28.2695), 'M+H', hypothesised, hypothesis(282)).

is_directly_defined_instance_of(e_compound(287.2104, 4.0216), t_compound_32598, hypothesised, hypothesis(288)).

has_adduct(e_compound(287.2104, 4.0216), 'M+H', hypothesised, hypothesis(288)).

is_directly_defined_instance_of(e_compound(287.2104, 4.0216), t_compound_32691, hypothesised, hypothesis(290)).

has_adduct(e_compound(287.2104, 4.0216), 'M+H', hypothesised, hypothesis(290)).

is_directly_defined_instance_of(e_compound(495.3352, 19.4689), t_compound_2628, hypothesised, hypothesis(296)).

has_adduct(e_compound(495.3352, 19.4689), 'M+H', hypothesised, hypothesis(296)).

is_directly_defined_instance_of(e_compound(495.3352, 19.4689), t_compound_2981, hypothesised, hypothesis(297)).

has_adduct(e_compound(495.3352, 19.4689), 'M+H', hypothesised, hypothesis(297)).

is_directly_defined_instance_of(e_compound(495.3352, 19.4689), t_compound_4290, hypothesised, hypothesis(298)).

has_adduct(e_compound(495.3352, 19.4689), 'M+H', hypothesised, hypothesis(298)).

is_directly_defined_instance_of(e_compound(495.3352, 19.4689), t_compound_2969, hypothesised, hypothesis(299)).

has_adduct(e_compound(495.3352, 19.4689), 'M+H', hypothesised, hypothesis(299)).

is_directly_defined_instance_of(e_compound(495.3352, 19.4689), t_compound_2940, hypothesised, hypothesis(304)).

has_adduct(e_compound(495.3352, 19.4689), 'M+H', hypothesised, hypothesis(304)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2950, hypothesised, hypothesis(312)).

has_adduct(e_compound(517.3172, 19.4689), 'M+H', hypothesised, hypothesis(312)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2988, hypothesised, hypothesis(313)).

has_adduct(e_compound(517.3172, 19.4689), 'M+H', hypothesised, hypothesis(313)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2628, hypothesised, hypothesis(315)).

has_adduct(e_compound(517.3172, 19.4689), 'M+Na', hypothesised, hypothesis(315)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2981, hypothesised, hypothesis(316)).

has_adduct(e_compound(517.3172, 19.4689), 'M+Na', hypothesised, hypothesis(316)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_4290, hypothesised, hypothesis(317)).

has_adduct(e_compound(517.3172, 19.4689), 'M+Na', hypothesised, hypothesis(317)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2969, hypothesised, hypothesis(318)).

has_adduct(e_compound(517.3172, 19.4689), 'M+Na', hypothesised, hypothesis(318)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_2940, hypothesised, hypothesis(323)).

has_adduct(e_compound(517.3172, 19.4689), 'M+Na', hypothesised, hypothesis(323)).

is_directly_defined_instance_of(e_compound(517.3172, 19.4689), t_compound_1414, hypothesised, hypothesis(326)).

has_adduct(e_compound(517.3172, 19.4689), 'M+H-H2O', hypothesised, hypothesis(326)).

is_directly_defined_instance_of(e_compound(547.3635, 21.5039), t_compound_2992, hypothesised, hypothesis(328)).

has_adduct(e_compound(547.3635, 21.5039), 'M+H', hypothesised, hypothesis(328)).

is_directly_defined_instance_of(e_compound(547.3635, 21.5039), t_compound_2693, hypothesised, hypothesis(329)).

has_adduct(e_compound(547.3635, 21.5039), 'M+H', hypothesised, hypothesis(329)).

is_directly_defined_instance_of(e_compound(571.3644, 20.9008), t_compound_2984, hypothesised, hypothesis(335)).

has_adduct(e_compound(571.3644, 20.9008), 'M+H', hypothesised, hypothesis(335)).

is_directly_defined_instance_of(e_compound(569.3479, 18.8524), t_compound_3001, hypothesised, hypothesis(342)).

has_adduct(e_compound(569.3479, 18.8524), 'M+H', hypothesised, hypothesis(342)).

is_directly_defined_instance_of(e_compound(569.3479, 18.8524), t_compound_3002, hypothesised, hypothesis(343)).

has_adduct(e_compound(569.3479, 18.8524), 'M+H', hypothesised, hypothesis(343)).

is_directly_defined_instance_of(e_compound(567.3328, 17.8636), t_compound_2960, hypothesised, hypothesis(351)).

has_adduct(e_compound(567.3328, 17.8636), 'M+H', hypothesised, hypothesis(351)).

is_directly_defined_instance_of(e_compound(567.3328, 17.8636), t_compound_2993, hypothesised, hypothesis(356)).

has_adduct(e_compound(567.3328, 17.8636), 'M+Na', hypothesised, hypothesis(356)).

is_directly_defined_instance_of(e_compound(567.3328, 17.8636), t_compound_2998, hypothesised, hypothesis(357)).

has_adduct(e_compound(567.3328, 17.8636), 'M+Na', hypothesised, hypothesis(357)).

is_directly_defined_instance_of(e_compound(589.3148, 17.8636), t_compound_2960, hypothesised, hypothesis(365)).

has_adduct(e_compound(589.3148, 17.8636), 'M+Na', hypothesised, hypothesis(365)).

is_directly_defined_instance_of(e_compound(589.3148, 17.8636), t_compound_1400, hypothesised, hypothesis(367)).

has_adduct(e_compound(589.3148, 17.8636), 'M+K', hypothesised, hypothesis(367)).

is_directly_defined_instance_of(e_compound(589.3148, 17.8636), t_compound_1426, hypothesised, hypothesis(368)).

has_adduct(e_compound(589.3148, 17.8636), 'M+K', hypothesised, hypothesis(368)).

is_directly_defined_instance_of(e_compound(481.3167, 17.4164), t_compound_2939, hypothesised, hypothesis(383)).

has_adduct(e_compound(481.3167, 17.4164), 'M+H', hypothesised, hypothesis(383)).

is_directly_defined_instance_of(e_compound(481.3167, 17.4164), t_compound_4264, hypothesised, hypothesis(384)).

has_adduct(e_compound(481.3167, 17.4164), 'M+H', hypothesised, hypothesis(384)).

is_directly_defined_instance_of(e_compound(481.3167, 17.4164), t_compound_4300, hypothesised, hypothesis(385)).

has_adduct(e_compound(481.3167, 17.4164), 'M+H', hypothesised, hypothesis(385)).

is_directly_defined_instance_of(e_compound(477.2858, 16.6885), t_compound_4273, hypothesised, hypothesis(391)).

has_adduct(e_compound(477.2858, 16.6885), 'M+H', hypothesised, hypothesis(391)).

is_directly_defined_instance_of(e_compound(477.2858, 16.6885), t_compound_4303, hypothesised, hypothesis(392)).

has_adduct(e_compound(477.2858, 16.6885), 'M+H', hypothesised, hypothesis(392)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_4289, hypothesised, hypothesis(401)).

has_adduct(e_compound(499.2672, 16.6885), 'M+H', hypothesised, hypothesis(401)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_4315, hypothesised, hypothesis(402)).

has_adduct(e_compound(499.2672, 16.6885), 'M+H', hypothesised, hypothesis(402)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_4273, hypothesised, hypothesis(404)).

has_adduct(e_compound(499.2672, 16.6885), 'M+Na', hypothesised, hypothesis(404)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_4303, hypothesised, hypothesis(405)).

has_adduct(e_compound(499.2672, 16.6885), 'M+Na', hypothesised, hypothesis(405)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_9125, hypothesised, hypothesis(410)).

has_adduct(e_compound(499.2672, 16.6885), 'M+NH4', hypothesised, hypothesis(410)).

is_directly_defined_instance_of(e_compound(499.2672, 16.6885), t_compound_136330, hypothesised, hypothesis(412)).

has_adduct(e_compound(499.2672, 16.6885), 'M+NH4', hypothesised, hypothesis(412)).

is_directly_defined_instance_of(e_compound(501.286, 17.7652), t_compound_4271, hypothesised, hypothesis(415)).

has_adduct(e_compound(501.286, 17.7652), 'M+H', hypothesised, hypothesis(415)).

is_directly_defined_instance_of(e_compound(501.286, 17.7652), t_compound_4313, hypothesised, hypothesis(416)).

has_adduct(e_compound(501.286, 17.7652), 'M+H', hypothesised, hypothesis(416)).

is_directly_defined_instance_of(e_compound(501.286, 17.7652), t_compound_4314, hypothesised, hypothesis(417)).

has_adduct(e_compound(501.286, 17.7652), 'M+H', hypothesised, hypothesis(417)).

is_directly_defined_instance_of(e_compound(501.286, 17.7652), t_compound_4329, hypothesised, hypothesis(418)).

has_adduct(e_compound(501.286, 17.7652), 'M+H', hypothesised, hypothesis(418)).

is_directly_defined_instance_of(e_compound(525.2853, 17.6984), t_compound_4275, hypothesised, hypothesis(426)).

has_adduct(e_compound(525.2853, 17.6984), 'M+H', hypothesised, hypothesis(426)).

is_directly_defined_instance_of(e_compound(525.2853, 17.6984), t_compound_4322, hypothesised, hypothesis(427)).

has_adduct(e_compound(525.2853, 17.6984), 'M+H', hypothesised, hypothesis(427)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_5539, hypothesised, hypothesis(433)).

has_adduct(e_compound(511.3265, 16.2547), 'M+H', hypothesised, hypothesis(433)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_34151, hypothesised, hypothesis(434)).

has_adduct(e_compound(511.3265, 16.2547), 'M+H', hypothesised, hypothesis(434)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_39064, hypothesised, hypothesis(438)).

has_adduct(e_compound(511.3265, 16.2547), 'M+NH4', hypothesised, hypothesis(438)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_109590, hypothesised, hypothesis(439)).

has_adduct(e_compound(511.3265, 16.2547), 'M+NH4', hypothesised, hypothesis(439)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_118622, hypothesised, hypothesis(440)).

has_adduct(e_compound(511.3265, 16.2547), 'M+NH4', hypothesised, hypothesis(440)).

is_directly_defined_instance_of(e_compound(511.3265, 16.2547), t_compound_98957, hypothesised, hypothesis(441)).

has_adduct(e_compound(511.3265, 16.2547), 'M+NH4', hypothesised, hypothesis(441)).

is_directly_defined_instance_of(e_compound(533.3079, 16.2547), t_compound_34151, hypothesised, hypothesis(446)).

has_adduct(e_compound(533.3079, 16.2547), 'M+Na', hypothesised, hypothesis(446)).

is_directly_defined_instance_of(e_compound(533.3079, 16.2547), t_compound_5539, hypothesised, hypothesis(447)).

has_adduct(e_compound(533.3079, 16.2547), 'M+Na', hypothesised, hypothesis(447)).

is_directly_defined_instance_of(e_compound(533.3079, 16.2547), t_compound_5523, hypothesised, hypothesis(450)).

has_adduct(e_compound(533.3079, 16.2547), 'M+H-H2O', hypothesised, hypothesis(450)).

is_directly_defined_instance_of(e_compound(539.3571, 20.2721), t_compound_5538, hypothesised, hypothesis(451)).

has_adduct(e_compound(539.3571, 20.2721), 'M+H', hypothesised, hypothesis(451)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_87054, hypothesised, hypothesis(457)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(457)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_17697, hypothesised, hypothesis(458)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(458)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_17703, hypothesised, hypothesis(459)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(459)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_89945, hypothesised, hypothesis(460)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(460)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_32092, hypothesised, hypothesis(461)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(461)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_50813, hypothesised, hypothesis(462)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(462)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_17696, hypothesised, hypothesis(464)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(464)).

is_directly_defined_instance_of(e_compound(356.2923, 24.6907), t_compound_27564, hypothesised, hypothesis(465)).

has_adduct(e_compound(356.2923, 24.6907), 'M+H', hypothesised, hypothesis(465)).

is_directly_defined_instance_of(e_compound(129.1515, 1.9077), t_compound_33572, hypothesised, hypothesis(480)).

has_adduct(e_compound(129.1515, 1.9077), 'M+NH4', hypothesised, hypothesis(480)).

is_directly_defined_instance_of(e_compound(129.1515, 1.9077), t_compound_33616, hypothesised, hypothesis(486)).

has_adduct(e_compound(129.1515, 1.9077), 'M+NH4', hypothesised, hypothesis(486)).

is_directly_defined_instance_of(e_compound(129.1515, 1.9077), t_compound_33617, hypothesised, hypothesis(487)).

has_adduct(e_compound(129.1515, 1.9077), 'M+NH4', hypothesised, hypothesis(487)).

is_directly_defined_instance_of(e_compound(129.1515, 1.9077), t_compound_33618, hypothesised, hypothesis(488)).

has_adduct(e_compound(129.1515, 1.9077), 'M+NH4', hypothesised, hypothesis(488)).

is_directly_defined_instance_of(e_compound(129.1515, 1.9077), t_compound_33638, hypothesised, hypothesis(489)).

has_adduct(e_compound(129.1515, 1.9077), 'M+NH4', hypothesised, hypothesis(489)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31494, hypothesised, hypothesis(491)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(491)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31495, hypothesised, hypothesis(492)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(492)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31496, hypothesised, hypothesis(493)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(493)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31497, hypothesised, hypothesis(494)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(494)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31498, hypothesised, hypothesis(495)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(495)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31512, hypothesised, hypothesis(496)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(496)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31513, hypothesised, hypothesis(497)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(497)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31514, hypothesised, hypothesis(498)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(498)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31516, hypothesised, hypothesis(499)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(499)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31517, hypothesised, hypothesis(500)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(500)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31518, hypothesised, hypothesis(501)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(501)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31519, hypothesised, hypothesis(502)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(502)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25640, hypothesised, hypothesis(503)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(503)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25641, hypothesised, hypothesis(504)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(504)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31529, hypothesised, hypothesis(505)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(505)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25642, hypothesised, hypothesis(506)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(506)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_27447, hypothesised, hypothesis(507)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(507)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_26465, hypothesised, hypothesis(508)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(508)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_26466, hypothesised, hypothesis(509)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(509)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25956, hypothesised, hypothesis(510)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(510)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25957, hypothesised, hypothesis(511)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(511)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25958, hypothesised, hypothesis(512)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(512)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25959, hypothesised, hypothesis(513)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(513)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25960, hypothesised, hypothesis(514)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(514)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25961, hypothesised, hypothesis(515)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(515)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_26479, hypothesised, hypothesis(516)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(516)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25742, hypothesised, hypothesis(517)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(517)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25743, hypothesised, hypothesis(518)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(518)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25744, hypothesised, hypothesis(519)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(519)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25745, hypothesised, hypothesis(520)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(520)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25746, hypothesised, hypothesis(521)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(521)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25747, hypothesised, hypothesis(522)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(522)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25748, hypothesised, hypothesis(523)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(523)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25750, hypothesised, hypothesis(524)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(524)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25751, hypothesised, hypothesis(525)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(525)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25496, hypothesised, hypothesis(526)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(526)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25752, hypothesised, hypothesis(527)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(527)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25754, hypothesised, hypothesis(528)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(528)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25755, hypothesised, hypothesis(529)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(529)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25758, hypothesised, hypothesis(530)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(530)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25759, hypothesised, hypothesis(531)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(531)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25760, hypothesised, hypothesis(532)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(532)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31648, hypothesised, hypothesis(533)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(533)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25761, hypothesised, hypothesis(534)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(534)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31649, hypothesised, hypothesis(535)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(535)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31651, hypothesised, hypothesis(536)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(536)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31653, hypothesised, hypothesis(537)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(537)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_32245, hypothesised, hypothesis(538)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(538)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25955, hypothesised, hypothesis(540)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(540)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25749, hypothesised, hypothesis(541)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(541)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25757, hypothesised, hypothesis(542)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(542)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_31784, hypothesised, hypothesis(543)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(543)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25684, hypothesised, hypothesis(544)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(544)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_26464, hypothesised, hypothesis(545)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(545)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25753, hypothesised, hypothesis(546)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(546)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_25756, hypothesised, hypothesis(547)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(547)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_180634, hypothesised, hypothesis(548)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(548)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_27471, hypothesised, hypothesis(550)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(550)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_27479, hypothesised, hypothesis(551)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(551)).

is_directly_defined_instance_of(e_compound(282.2563, 31.2616), t_compound_26617, hypothesised, hypothesis(559)).

has_adduct(e_compound(282.2563, 31.2616), 'M+H', hypothesised, hypothesis(559)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_27471, hypothesised, hypothesis(573)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(573)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_27479, hypothesised, hypothesis(574)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(574)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_26617, hypothesised, hypothesis(582)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(582)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25955, hypothesised, hypothesis(591)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(591)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25749, hypothesised, hypothesis(592)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(592)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25757, hypothesised, hypothesis(593)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(593)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31784, hypothesised, hypothesis(594)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(594)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25684, hypothesised, hypothesis(595)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(595)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_26464, hypothesised, hypothesis(596)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(596)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25753, hypothesised, hypothesis(597)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(597)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25756, hypothesised, hypothesis(598)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(598)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_180634, hypothesised, hypothesis(599)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(599)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31494, hypothesised, hypothesis(600)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(600)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31495, hypothesised, hypothesis(601)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(601)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31496, hypothesised, hypothesis(602)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(602)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31497, hypothesised, hypothesis(603)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(603)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31498, hypothesised, hypothesis(604)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(604)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31512, hypothesised, hypothesis(605)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(605)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31513, hypothesised, hypothesis(606)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(606)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31514, hypothesised, hypothesis(607)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(607)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31516, hypothesised, hypothesis(608)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(608)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31517, hypothesised, hypothesis(609)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(609)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31518, hypothesised, hypothesis(610)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(610)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31519, hypothesised, hypothesis(611)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(611)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25640, hypothesised, hypothesis(612)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(612)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25641, hypothesised, hypothesis(613)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(613)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31529, hypothesised, hypothesis(614)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(614)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25642, hypothesised, hypothesis(615)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(615)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_27447, hypothesised, hypothesis(616)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(616)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_26465, hypothesised, hypothesis(617)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(617)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_26466, hypothesised, hypothesis(618)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(618)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25956, hypothesised, hypothesis(619)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(619)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25957, hypothesised, hypothesis(620)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(620)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25958, hypothesised, hypothesis(621)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(621)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25959, hypothesised, hypothesis(622)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(622)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25960, hypothesised, hypothesis(623)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(623)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25961, hypothesised, hypothesis(624)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(624)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_26479, hypothesised, hypothesis(625)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(625)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25742, hypothesised, hypothesis(626)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(626)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25743, hypothesised, hypothesis(627)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(627)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25744, hypothesised, hypothesis(628)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(628)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25745, hypothesised, hypothesis(629)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(629)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25746, hypothesised, hypothesis(630)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(630)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25747, hypothesised, hypothesis(631)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(631)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25748, hypothesised, hypothesis(632)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(632)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25750, hypothesised, hypothesis(633)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(633)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25751, hypothesised, hypothesis(634)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(634)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25496, hypothesised, hypothesis(635)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(635)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25752, hypothesised, hypothesis(636)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(636)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25754, hypothesised, hypothesis(637)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(637)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25755, hypothesised, hypothesis(638)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(638)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25758, hypothesised, hypothesis(639)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(639)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25759, hypothesised, hypothesis(640)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(640)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25760, hypothesised, hypothesis(641)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(641)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31648, hypothesised, hypothesis(642)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(642)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_25761, hypothesised, hypothesis(643)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(643)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31649, hypothesised, hypothesis(644)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(644)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31651, hypothesised, hypothesis(645)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(645)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_31653, hypothesised, hypothesis(646)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(646)).

is_directly_defined_instance_of(e_compound(264.2458, 31.2616), t_compound_32245, hypothesised, hypothesis(647)).

has_adduct(e_compound(264.2458, 31.2616), 'M+H-H2O', hypothesised, hypothesis(647)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31751, hypothesised, hypothesis(653)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(653)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25623, hypothesised, hypothesis(654)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(654)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31591, hypothesised, hypothesis(655)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(655)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25458, hypothesised, hypothesis(656)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(656)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25459, hypothesised, hypothesis(657)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(657)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25460, hypothesised, hypothesis(658)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(658)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25461, hypothesised, hypothesis(659)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(659)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25462, hypothesised, hypothesis(660)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(660)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25463, hypothesised, hypothesis(661)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(661)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25464, hypothesised, hypothesis(662)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(662)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25465, hypothesised, hypothesis(663)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(663)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25466, hypothesised, hypothesis(664)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(664)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25467, hypothesised, hypothesis(665)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(665)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25482, hypothesised, hypothesis(666)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(666)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31636, hypothesised, hypothesis(667)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(667)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31141, hypothesised, hypothesis(668)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(668)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25559, hypothesised, hypothesis(669)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(669)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25334, hypothesised, hypothesis(670)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(670)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25335, hypothesised, hypothesis(671)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(671)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31482, hypothesised, hypothesis(672)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(672)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31575, hypothesised, hypothesis(673)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(673)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31600, hypothesised, hypothesis(674)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(674)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31607, hypothesised, hypothesis(675)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(675)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31619, hypothesised, hypothesis(676)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(676)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25248, hypothesised, hypothesis(677)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(677)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31905, hypothesised, hypothesis(678)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(678)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25532, hypothesised, hypothesis(679)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(679)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_31942, hypothesised, hypothesis(680)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(680)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_25306, hypothesised, hypothesis(681)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(681)).

is_directly_defined_instance_of(e_compound(256.2406, 30.8166), t_compound_180598, hypothesised, hypothesis(682)).

has_adduct(e_compound(256.2406, 30.8166), 'M+H', hypothesised, hypothesis(682)).

is_directly_defined_instance_of(e_compound(255.2571, 26.94), t_compound_32717, hypothesised, hypothesis(691)).

has_adduct(e_compound(255.2571, 26.94), 'M+H', hypothesised, hypothesis(691)).

is_directly_defined_instance_of(e_compound(648.5165, 31.91), t_compound_33990, hypothesised, hypothesis(697)).

has_adduct(e_compound(648.5165, 31.91), 'M+H', hypothesised, hypothesis(697)).

is_directly_defined_instance_of(e_compound(648.5165, 31.91), t_compound_33992, hypothesised, hypothesis(698)).

has_adduct(e_compound(648.5165, 31.91), 'M+H', hypothesised, hypothesis(698)).

is_directly_defined_instance_of(e_compound(648.5165, 31.91), t_compound_34410, hypothesised, hypothesis(699)).

has_adduct(e_compound(648.5165, 31.91), 'M+H', hypothesised, hypothesis(699)).

is_directly_defined_instance_of(e_compound(648.5165, 31.91), t_compound_18287, hypothesised, hypothesis(700)).

has_adduct(e_compound(648.5165, 31.91), 'M+H', hypothesised, hypothesis(700)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_34345, hypothesised, hypothesis(706)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(706)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_34429, hypothesised, hypothesis(707)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(707)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_34440, hypothesised, hypothesis(708)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(708)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_33999, hypothesised, hypothesis(709)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(709)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_34001, hypothesised, hypothesis(710)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(710)).

is_directly_defined_instance_of(e_compound(646.5048, 30.4889), t_compound_34006, hypothesised, hypothesis(711)).

has_adduct(e_compound(646.5048, 30.4889), 'M+H', hypothesised, hypothesis(711)).

is_directly_defined_instance_of(e_compound(672.5205, 32.6973), t_compound_34367, hypothesised, hypothesis(717)).

has_adduct(e_compound(672.5205, 32.6973), 'M+H', hypothesised, hypothesis(717)).

is_directly_defined_instance_of(e_compound(672.5205, 32.6973), t_compound_34465, hypothesised, hypothesis(718)).

has_adduct(e_compound(672.5205, 32.6973), 'M+H', hypothesised, hypothesis(718)).

is_directly_defined_instance_of(e_compound(672.5205, 32.6973), t_compound_34469, hypothesised, hypothesis(719)).

has_adduct(e_compound(672.5205, 32.6973), 'M+H', hypothesised, hypothesis(719)).

is_directly_defined_instance_of(e_compound(672.5205, 32.6973), t_compound_34481, hypothesised, hypothesis(720)).

has_adduct(e_compound(672.5205, 32.6973), 'M+H', hypothesised, hypothesis(720)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_32659, hypothesised, hypothesis(727)).

has_adduct(e_compound(425.3504, 19.5463), 'M+H', hypothesised, hypothesis(727)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_32692, hypothesised, hypothesis(731)).

has_adduct(e_compound(425.3504, 19.5463), 'M+H', hypothesised, hypothesis(731)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_130330, hypothesised, hypothesis(732)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(732)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_47438, hypothesised, hypothesis(733)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(733)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_54119, hypothesised, hypothesis(734)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(734)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_103840, hypothesised, hypothesis(735)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(735)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_90045, hypothesised, hypothesis(736)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(736)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_109570, hypothesised, hypothesis(737)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(737)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_90372, hypothesised, hypothesis(738)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(738)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_67848, hypothesised, hypothesis(739)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(739)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_90632, hypothesised, hypothesis(740)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(740)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_135948, hypothesised, hypothesis(741)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(741)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_82446, hypothesised, hypothesis(742)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(742)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_110350, hypothesised, hypothesis(743)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(743)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_113679, hypothesised, hypothesis(744)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(744)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_55056, hypothesised, hypothesis(745)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(745)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_50195, hypothesised, hypothesis(746)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(746)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_90901, hypothesised, hypothesis(747)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(747)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_60441, hypothesised, hypothesis(748)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(748)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_86561, hypothesised, hypothesis(749)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(749)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_70953, hypothesised, hypothesis(750)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(750)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_86826, hypothesised, hypothesis(751)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(751)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_133167, hypothesised, hypothesis(752)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(752)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_74543, hypothesised, hypothesis(753)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(753)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_74551, hypothesised, hypothesis(754)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(754)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_101687, hypothesised, hypothesis(755)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(755)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_68153, hypothesised, hypothesis(756)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(756)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_92222, hypothesised, hypothesis(757)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(757)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_123456, hypothesised, hypothesis(758)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(758)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_148035, hypothesised, hypothesis(759)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(759)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_117059, hypothesised, hypothesis(760)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(760)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_95301, hypothesised, hypothesis(761)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(761)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_96331, hypothesised, hypothesis(762)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(762)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_105804, hypothesised, hypothesis(763)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(763)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_70221, hypothesised, hypothesis(764)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(764)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_108624, hypothesised, hypothesis(765)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(765)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_112722, hypothesised, hypothesis(766)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(766)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_119378, hypothesised, hypothesis(767)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(767)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_69204, hypothesised, hypothesis(768)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(768)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_80724, hypothesised, hypothesis(769)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(769)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_48980, hypothesised, hypothesis(770)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(770)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_96853, hypothesised, hypothesis(771)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(771)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_104791, hypothesised, hypothesis(772)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(772)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_55895, hypothesised, hypothesis(773)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(773)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_75610, hypothesised, hypothesis(774)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(774)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_56411, hypothesised, hypothesis(775)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(775)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_62555, hypothesised, hypothesis(776)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(776)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_49756, hypothesised, hypothesis(777)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(777)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_65629, hypothesised, hypothesis(778)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(778)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_144224, hypothesised, hypothesis(779)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(779)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_50784, hypothesised, hypothesis(780)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(780)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_94565, hypothesised, hypothesis(781)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(781)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_144489, hypothesised, hypothesis(782)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(782)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_60268, hypothesised, hypothesis(783)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(783)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_149101, hypothesised, hypothesis(784)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(784)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_122735, hypothesised, hypothesis(785)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(785)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_134258, hypothesised, hypothesis(786)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(786)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_87923, hypothesised, hypothesis(787)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(787)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_52339, hypothesised, hypothesis(788)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(788)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_61299, hypothesised, hypothesis(789)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(789)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_122742, hypothesised, hypothesis(790)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(790)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_142456, hypothesised, hypothesis(791)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(791)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_148090, hypothesised, hypothesis(792)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(792)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_142463, hypothesised, hypothesis(793)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(793)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_79487, hypothesised, hypothesis(794)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(794)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_88703, hypothesised, hypothesis(795)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(795)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_98943, hypothesised, hypothesis(796)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(796)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_111743, hypothesised, hypothesis(797)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(797)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_70022, hypothesised, hypothesis(798)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(798)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_119951, hypothesised, hypothesis(799)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(799)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_93341, hypothesised, hypothesis(800)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(800)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_78238, hypothesised, hypothesis(801)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(801)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_45727, hypothesised, hypothesis(802)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(802)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_72100, hypothesised, hypothesis(803)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(803)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_45479, hypothesised, hypothesis(804)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(804)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_87721, hypothesised, hypothesis(805)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(805)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_60586, hypothesised, hypothesis(806)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(806)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_113324, hypothesised, hypothesis(807)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(807)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_52399, hypothesised, hypothesis(808)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(808)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_108208, hypothesised, hypothesis(809)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(809)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_148659, hypothesised, hypothesis(810)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(810)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_64691, hypothesised, hypothesis(811)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(811)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_143029, hypothesised, hypothesis(812)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(812)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_103608, hypothesised, hypothesis(813)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(813)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_138939, hypothesised, hypothesis(814)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(814)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_86715, hypothesised, hypothesis(815)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(815)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_147645, hypothesised, hypothesis(816)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(816)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_89791, hypothesised, hypothesis(817)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(817)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_91839, hypothesised, hypothesis(818)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(818)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_43461, hypothesised, hypothesis(819)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(819)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_45767, hypothesised, hypothesis(820)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(820)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_42952, hypothesised, hypothesis(821)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(821)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_122825, hypothesised, hypothesis(822)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(822)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_134347, hypothesised, hypothesis(823)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(823)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_62927, hypothesised, hypothesis(824)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(824)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_68817, hypothesised, hypothesis(825)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(825)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_80850, hypothesised, hypothesis(826)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(826)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_52948, hypothesised, hypothesis(827)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(827)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_87777, hypothesised, hypothesis(828)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(828)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_52193, hypothesised, hypothesis(829)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(829)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_134114, hypothesised, hypothesis(830)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(830)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_73458, hypothesised, hypothesis(831)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(831)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_63987, hypothesised, hypothesis(832)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(832)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_95478, hypothesised, hypothesis(833)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(833)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_127991, hypothesised, hypothesis(834)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(834)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_145658, hypothesised, hypothesis(835)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(835)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_49661, hypothesised, hypothesis(836)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(836)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_50431, hypothesised, hypothesis(837)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(837)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22535, hypothesised, hypothesis(838)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(838)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23048, hypothesised, hypothesis(839)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(839)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21769, hypothesised, hypothesis(840)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(840)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18455, hypothesised, hypothesis(841)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(841)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21274, hypothesised, hypothesis(842)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(842)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18459, hypothesised, hypothesis(843)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(843)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21787, hypothesised, hypothesis(844)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(844)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23067, hypothesised, hypothesis(845)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(845)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22559, hypothesised, hypothesis(846)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(846)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21804, hypothesised, hypothesis(847)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(847)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24108, hypothesised, hypothesis(848)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(848)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23085, hypothesised, hypothesis(849)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(849)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21295, hypothesised, hypothesis(850)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(850)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24111, hypothesised, hypothesis(851)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(851)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18481, hypothesised, hypothesis(852)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(852)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18482, hypothesised, hypothesis(853)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(853)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23861, hypothesised, hypothesis(854)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(854)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22326, hypothesised, hypothesis(855)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(855)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21821, hypothesised, hypothesis(856)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(856)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23102, hypothesised, hypothesis(857)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(857)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18503, hypothesised, hypothesis(858)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(858)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_18504, hypothesised, hypothesis(859)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(859)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21836, hypothesised, hypothesis(860)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(860)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23119, hypothesised, hypothesis(861)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(861)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23383, hypothesised, hypothesis(862)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(862)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23134, hypothesised, hypothesis(863)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(863)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23404, hypothesised, hypothesis(864)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(864)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24187, hypothesised, hypothesis(865)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(865)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21886, hypothesised, hypothesis(866)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(866)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22149, hypothesised, hypothesis(867)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(867)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22662, hypothesised, hypothesis(868)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(868)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21896, hypothesised, hypothesis(869)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(869)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22680, hypothesised, hypothesis(870)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(870)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22171, hypothesised, hypothesis(871)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(871)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22940, hypothesised, hypothesis(872)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(872)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23711, hypothesised, hypothesis(873)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(873)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22697, hypothesised, hypothesis(874)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(874)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22192, hypothesised, hypothesis(875)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(875)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22964, hypothesised, hypothesis(876)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(876)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23733, hypothesised, hypothesis(877)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(877)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22714, hypothesised, hypothesis(878)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(878)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21187, hypothesised, hypothesis(879)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(879)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_22729, hypothesised, hypothesis(880)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(880)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23754, hypothesised, hypothesis(881)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(881)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24269, hypothesised, hypothesis(882)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(882)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24021, hypothesised, hypothesis(883)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(883)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_24024, hypothesised, hypothesis(884)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(884)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_20969, hypothesised, hypothesis(885)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(885)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21750, hypothesised, hypothesis(886)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(886)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_21240, hypothesised, hypothesis(887)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(887)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_23292, hypothesised, hypothesis(888)).

has_adduct(e_compound(425.3504, 19.5463), 'M+2H', hypothesised, hypothesis(888)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_32624, hypothesised, hypothesis(892)).

has_adduct(e_compound(425.3504, 19.5463), 'M+H-H2O', hypothesised, hypothesis(892)).

is_directly_defined_instance_of(e_compound(425.3504, 19.5463), t_compound_32639, hypothesised, hypothesis(893)).

has_adduct(e_compound(425.3504, 19.5463), 'M+H-H2O', hypothesised, hypothesis(893)).

