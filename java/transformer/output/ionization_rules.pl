% Automatically generated from transformer.jar (see javadoc in transformer/html)

ionization_inference_rule(_Modifier,1, fa, fa01, _SubClass, 'M+H', very_likely).

ionization_inference_rule(_Modifier,1, fa, fa01, _SubClass, 'M+H-H2O', very_likely).

ionization_inference_rule(_Modifier,2, fa, fa01, _SubClass, 'M-H', very_likely).

ionization_inference_rule(_Modifier,1, fa, fa01, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,2, fa, fa01, _SubClass, 'M-H-H2O', likely).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(Modifier, 2, gp, gp01, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104),
	(Modifier = 1; Modifier = 2; Modifier = 4).

ionization_inference_rule(Modifier, 2, gp, gp01, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+K', likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+H-H2O', very_unlikely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M-H', very_unlikely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M-H-H2O', very_unlikely) :- 
	(SubClass = gp0101; SubClass = gp0102; SubClass = gp0108; SubClass = gp0103; SubClass = gp0109; SubClass = gp0104).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+H-H2O', very_likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(Modifier, 2, gp, gp01, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp01, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 1, gp, gp01, SubClass, 'M+K', likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M-H-H2O', likely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 2, gp, gp01, SubClass, 'M-H', very_unlikely) :- 
	(SubClass = gp0105; SubClass = gp0106; SubClass = gp0107).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(Modifier, 2, gp, gp02, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp02, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+K', likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+H-H2O', very_unlikely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M-H-H2O', very_unlikely) :- 
	(SubClass = gp0201; SubClass = gp0202; SubClass = gp0208; SubClass = gp0203; SubClass = gp0204).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(Modifier, 2, gp, gp02, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp02, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+K', likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 1, gp, gp02, SubClass, 'M+H-H2O', likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 2, gp, gp02, SubClass, 'M-H-H2O', likely) :- 
	(SubClass = gp0205; SubClass = gp0206; SubClass = gp0207).

ionization_inference_rule(_Modifier, 2, gp, gp06, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0601; SubClass = gp0602; SubClass = gp0603; SubClass = gp0604).

ionization_inference_rule(_Modifier, 1, gp, gp06, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0601; SubClass = gp0602; SubClass = gp0603; SubClass = gp0604).

ionization_inference_rule(_Modifier, 1, gp, gp06, SubClass, 'M+K', likely) :- 
	(SubClass = gp0601; SubClass = gp0602; SubClass = gp0603; SubClass = gp0604).

ionization_inference_rule(_Modifier, 1, gp, gp06, SubClass, 'M+H', very_unlikely) :- 
	(SubClass = gp0601; SubClass = gp0602; SubClass = gp0603; SubClass = gp0604).

ionization_inference_rule(_Modifier, 1, gp, gp06, SubClass, 'M+H-H2O', very_unlikely) :- 
	(SubClass = gp0601; SubClass = gp0602; SubClass = gp0603; SubClass = gp0604).

ionization_inference_rule(_Modifier, 2, gp, gp04, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404).

ionization_inference_rule(Modifier, 2, gp, gp04, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp04, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 2, gp, gp04, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404).

ionization_inference_rule(_Modifier, 1, gp, gp04, SubClass, _Adduct, very_unlikely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404).

ionization_inference_rule(_Modifier, 2, gp, gp04, SubClass, 'M-H-H2O', very_unlikely) :- 
	(SubClass = gp0401; SubClass = gp0402; SubClass = gp0411; SubClass = gp0403; SubClass = gp0404).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(_Modifier, 2, gp, gp03, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(Modifier, 2, gp, gp03, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp03, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+K', likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(_Modifier, 2, gp, gp03, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M-H-H2O', very_unlikely) :- 
	(SubClass = gp0301; SubClass = gp0302; SubClass = gp0303; SubClass = gp0304).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+H', very_likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+H-H2O', very_likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(_Modifier, 2, gp, gp03, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(Modifier, 2, gp, gp03, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp03, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+Na', likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(_Modifier, 1, gp, gp03, SubClass, 'M+K', likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(_Modifier, 2, gp, gp03, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp0305; SubClass = gp0306; SubClass = gp0307).

ionization_inference_rule(_Modifier, 2, gp, gp10, SubClass, 'M-H', very_likely) :- 
	(SubClass = gp1001; SubClass = gp1002; SubClass = gp1003; SubClass = gp1004).

ionization_inference_rule(Modifier, 2, gp, gp10, SubClass, 'M+HCOO-', very_likely) :- 
	(SubClass = gp1001; SubClass = gp1002; SubClass = gp1003; SubClass = gp1004),
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, gp, gp10, SubClass, 'M+CH3COO-', very_likely) :- 
	(SubClass = gp1001; SubClass = gp1002; SubClass = gp1003; SubClass = gp1004),
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier, 2, gp, gp10, SubClass, 'M+Cl-', likely) :- 
	(SubClass = gp1001; SubClass = gp1002; SubClass = gp1003; SubClass = gp1004).

ionization_inference_rule(_Modifier, 1, gp, gp10, SubClass, _Adduct, very_unlikely) :- 
	(SubClass = gp1001; SubClass = gp1002; SubClass = gp1003; SubClass = gp1004).

ionization_inference_rule(_Modifier,1, gl, gl01, _SubClass, 'M+H', very_likely).

ionization_inference_rule(Modifier, 1, gl, gl01, _SubClass, 'M+NH4', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, gl, gl01, _SubClass, 'M+NH4', very_unlikely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(_Modifier,1, gl, gl01, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,2, gl, gl01, _SubClass, _Adduct, very_unlikely).

ionization_inference_rule(Modifier, 1, gl, gl02, _SubClass, 'M+NH4', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, gl, gl02, _SubClass, 'M+NH4', very_unlikely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(_Modifier,1, gl, gl02, _SubClass, 'M+Na', likely).

ionization_inference_rule(Modifier, 1, gl, gl02, _SubClass, 'M+H', likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, gl, gl02, _SubClass, 'M+H', very_unlikely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(_Modifier,2, gl, gl02, _SubClass, _Adduct, very_unlikely).

ionization_inference_rule(Modifier, 1, gl, gl03, _SubClass, 'M+NH4', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, gl, gl03, _SubClass, 'M+NH4', very_unlikely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(_Modifier,1, gl, gl03, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,1, gl, gl03, _SubClass, 'M+H', very_unlikely).

ionization_inference_rule(_Modifier,2, gl, gl03, _SubClass, _Adduct, very_unlikely).

ionization_inference_rule(_Modifier,1, sp, sp02, _SubClass, 'M+H', very_likely).

ionization_inference_rule(_Modifier,2, sp, sp02, _SubClass, 'M-H', very_likely).

ionization_inference_rule(Modifier, 2, sp, sp02, _SubClass, 'M+HCOO-', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, sp, sp02, _SubClass, 'M+CH3COO-', very_likely) :- 
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier,1, sp, sp02, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,2, sp, sp02, _SubClass, 'M+Cl-', likely).

ionization_inference_rule(_Modifier,1, sp, sp03, _SubClass, 'M+H', very_likely).

ionization_inference_rule(Modifier, 2, sp, sp03, _SubClass, 'M+HCOO-', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 2).

ionization_inference_rule(Modifier, 2, sp, sp03, _SubClass, 'M+CH3COO-', very_likely) :- 
	(Modifier = 1; Modifier = 5; Modifier = 3).

ionization_inference_rule(_Modifier,1, sp, sp03, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,1, sp, sp03, _SubClass, 'M+K', likely).

ionization_inference_rule(_Modifier,2, sp, sp03, _SubClass, 'M+Cl-', likely).

ionization_inference_rule(_Modifier,2, sp, sp03, _SubClass, 'M-H', very_unlikely).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+NH4', very_likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+NH4', very_unlikely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+H', likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+H', very_likely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+H-H2O', likely) :- 
	(Modifier = 1; Modifier = 4; Modifier = 5).

ionization_inference_rule(Modifier, 1, st, st01, _SubClass, 'M+H-H2O', very_likely) :- 
	(Modifier = 2; Modifier = 3).

ionization_inference_rule(_Modifier,1, st, st01, _SubClass, 'M+Na', likely).

ionization_inference_rule(_Modifier,2, st, st01, _SubClass, _Adduct, very_unlikely).

