package org.lis.generation.generators;

import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import org.lis.domain.EmpiricalCompound;
import org.lis.domain.Experiment;
import org.lis.domain.PutativeAnnotation;
import org.lis.domain.IonizationMode;
import org.lis.domain.Modifier;
import org.lis.domain.TheoreticalCompound;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;

/**
 * This class is the responsible for generating the tests of the Prolog
 * knowledge base from the spreadsheet, as well as a JSON file to test
 * the API Rest.
 */
public class TestGenerator 
{
    //This table is used to translate certainty levels from the original
    //in the spreadsheet (possible, expected, etc.) to requered ones
    //(likely, very likely, etc.).
    private static HashMap<String, String> trueValueMap = new HashMap<>();

    /**
     * This method allows generating the tests from the spreadsheet
     * <i>resources/input.ods</i> into the files <i>output/input_facts.pl</i>
     * and <i>output/kb_test.pl</i> and <i>output/input.json</i>. Each element
     * of the array stored in this file follows the format established in the
     * class <i>org.lis.domain.PutativeAnnotation</i>). The module <i>kb_tests.pl</i>
     * consults <i>input_facts.pl</i>. The spreadsheet has
     * the data of a particular experiment.
     */
    public static void generate()
    {
        System.out.println("Generating tests..."); 
        try
        {
            trueValueMap.put("impossible", "very_unlikely");
            trueValueMap.put("improbable", "unlikely");
            trueValueMap.put("possible", "likely");
            trueValueMap.put("expected", "very_likely");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/compounds_db?serverTimezone=Europe/Madrid", "root", "socovos");
            Statement stmt = con.createStatement();
            FileWriter fwInputs = new FileWriter(new File("output/input_facts.pl"));
            FileWriter fwTests = new FileWriter(new File("output/kb_test.pl"));
            FileWriter fwJSON = new FileWriter(new File("output/input.json"));
            generateFiles(fwJSON, fwInputs, fwTests, con, stmt);
            fwInputs.close();
            fwTests.close(); 
            fwJSON.close();
        }catch(SQLException | IOException e)
        {
            System.err.println(e);
        }
    }

    private static void generateFiles(FileWriter fwJSON, FileWriter fwInputs, FileWriter fwTests, Connection con, Statement stmt)
    {
        StringBuilder sbInput = new StringBuilder("% Automatically generated from transformer.jar (see javadoc in transformer/html)\n\n" +
                ":- discontiguous(has_adduct/4).\n" +
                ":- discontiguous(is_directly_defined_instance_of/4).\n" +
                ":- discontiguous(ionization/2).\n" +
                ":- discontiguous(modifier/2).\n\n" +
                //To establish a modifier, you can write ```modifier(1, sure).```, ```modifier(2, sure).```, ..., or ```modifier(5, sure).```
                "modifier(none, sure).\n\n" + 
                "ionization(1, sure).\n\n");
        StringBuilder sbTests = new StringBuilder("% Automatically generated from transformer.jar (see javadoc in transformer/html)\n\n" +
                "% To see passed tests: 'pass(T)'\n" +
                "% To see failed tests: 'test(T), \\+ pass(T)\n\n" +
                ":-consult('../english.pl').\n" +
                ":-consult('input_facts.pl').\n\n" +
                ":- discontiguous(test/1).\n" +
                ":- discontiguous(has_identity/4).\n" +
                ":- discontiguous(pass/1).\n");
        Set<PutativeAnnotation> hypothesisSet = new HashSet<>();
        //To establish a modifier, you can write ```Modifier.modifier_NH3```, ```modifier_HCOO```, ..., or ```modifier_CH3COONH3```
        //in the second parameter of the constuctor of the class ```Experiment```.
        Experiment experiment = new Experiment(1, null, IonizationMode.positive, hypothesisSet);
        try
        {
            final Sheet sheet = SpreadSheet.createFromFile(new File("resources/input.ods")).getSheet(0);
            for(int i = 3; i <= 893; i++)
                //for(int i = 3; i <= sheet.getUsedRange().getEndPoint().getY(); i++)
            {
                String identifier = sheet.getCellAt("C"+i).getTextValue();
                if (isLipid(identifier, con, stmt)) generateFact(hypothesisSet, sbInput, sbTests, sheet, identifier, i);
            }
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonExperiment= gson.toJson(experiment);
            fwJSON.write(jsonExperiment.replace("\\","")); 
            fwInputs.write(sbInput.toString());
            fwTests.write(sbTests.toString());
            //con.close();
        }catch(/*SQLException |*/ IOException e)
        {
            System.err.println();
        }
    }

    private static void generateFact(Set<PutativeAnnotation> hypothesisSet, StringBuilder sbInput, StringBuilder sbTests, Sheet sheet, String identifier, int i)
    {
        String experimentalMass = sheet.getCellAt("A" + i).getTextValue().replace(",", ".");
        String retentionTime = sheet.getCellAt("B"+ i).getTextValue().replace(",", ".");
        String adductInSS = sheet.getCellAt("D"+i).getTextValue();
        String adduct = "'" + adductInSS + "'";

        // GENERATION OF JSON OBJECT

        String name = sheet.getCellAt("G"+i).getTextValue();
        EmpiricalCompound empiricalCompound = new EmpiricalCompound(Double.parseDouble(experimentalMass), Double.parseDouble(retentionTime));
        TheoreticalCompound theoreticalCompound = new TheoreticalCompound(Integer.parseInt(identifier), name);
        PutativeAnnotation hypothesis = new PutativeAnnotation(i, empiricalCompound, theoreticalCompound, adductInSS);
        hypothesisSet.add(hypothesis);

        // GENERATION FOR input_facts.pl 
        String compound = "e_compound(" + experimentalMass + ", " + retentionTime + ")";
        String directClass = "t_compound_" +  identifier;

        sbInput.append("is_directly_defined_instance_of(" + compound + ", " + directClass + ", hypothesised, " +
                "hypothesis(" + i + ")).\n\n");

        sbInput.append("has_adduct(" + compound + ", " + adduct + ", hypothesised, " +
                "hypothesis(" + i + ")).\n\n");

        // GENERATION FOR kb_tests.pl
        String explanation_score_1 = "ionization_rule(_L)"; //There are different types of explanations according to the interaction to relation between adduct rules
        if (sheet.getCellAt("I"+i).getTextValue().equals("N/A"))
            generateNegativeTest(sheet, sbTests, i, compound, directClass, adduct, "score_1", explanation_score_1, "ionization_rule_does_not_apply");
        else generatePositiveTest(sheet, sbTests, i, "I", compound, directClass, adduct, "score_1", explanation_score_1, "ionization_rule_applies");

        String explanation_score_2 = "adduct_relation_rule([" + directClass + ", " + compound +", " + adduct + ", _Compound2, _Adduct2 | _R])";
        if (sheet.getCellAt("J"+i).getTextValue().equals("N/A"))
            generateNegativeTest(sheet, sbTests, i, compound, directClass, adduct, "score_2", explanation_score_2, "adduct_relation_rule_does_not_apply"); 
        else generatePositiveTest(sheet, sbTests, i, "J", compound, directClass, adduct, "score_2", explanation_score_2, "adduct_relation_rule_applies");

        String explanation_score_3 = "retention_time_rule([" + compound + ", " + directClass + ", _LipidType, _NCarbons1, _NDoubleBonds1, _Lfor, _Lagainst])"; 
        if (sheet.getCellAt("K"+i).getTextValue().equals("N/A"))
            generateNegativeTest(sheet, sbTests, i, compound, directClass, adduct, "score_3", explanation_score_3, "retention_time_rule_does_not_apply");
        else generatePositiveTest(sheet, sbTests, i, "K", compound, directClass, adduct, "score_3", explanation_score_3, "retention_time_rule_applies");
    }

    private static boolean isLipid(String compound, Connection con, Statement stmt)
    {
        try
        {
            ResultSet rs1 = stmt.executeQuery("select compound_id from compounds_lm_classification where compound_id = " + compound);
            boolean thereAreResults1 = rs1.next();
            ResultSet rs2 = stmt.executeQuery("select compound_id from compounds_lipids_classification where compound_id = " + compound);
            boolean thereAreResults2 = rs2.next();
            return thereAreResults1 || thereAreResults2;
        }catch(SQLException ex)
        {
            System.out.println("Watch out!: " + ex);
            return false;
        }
    }

    private static void generatePositiveTest(Sheet sheet, StringBuilder sbTests, int i, String column, String compound, 
            String directClass, String adduct, String score, String explanation, String typeOfTest)
    {
        String idTest = "test(" + i + ", " + compound + ", " + directClass + ", " + adduct + ", " + typeOfTest + ")"; 
        sbTests.append("\n\ntest(" + idTest + ").\n");
        sbTests.append("pass(" + idTest + ") :-\n" + 
                "    has_identity(" + i + ", " + compound + ", " + directClass + ", " + trueValueMap.get(sheet.getCellAt(column+i).getTextValue()) + ", " + explanation + ")."); 
    }

    private static void generateNegativeTest(Sheet sheet, StringBuilder sbTests, int i, String compound, 
            String directClass, String adduct, String score, String explanation, String typeOfTest)
    {
        String idTest = "test(" + i + ", " + compound + ", " + directClass + ", " + adduct + ", " + typeOfTest + ")"; 
        sbTests.append("\n\ntest(" + idTest + ").\n");
        sbTests.append("pass(" + idTest + ") :-\n" + 
                "    \\+ has_identity(" + i + ", " + compound + ", " + directClass + ", _TrueValue, " + explanation + ")."); 
    }
}
