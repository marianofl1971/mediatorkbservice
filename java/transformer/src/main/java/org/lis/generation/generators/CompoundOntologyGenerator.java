package org.lis.generation.generators;

import org.lis.domain.TheoreticalCompound;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.Map;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class is the responsible for generating the compound ontology from the
 * Mediator database, as well as for generating a JSON file associating each
 * CEU Mass Mediator (CMM) identifier with the name of the compound. The JSON file
 * (<i>output/names.json</i>) will be useful so that the Java wrapper is independent
 * from CMM. Each element of the JSON array is supported by the
 * <i>org.lis.domain.TheoreticalCompound</i> class.
 */
public class CompoundOntologyGenerator
{
    /**
     * This method allows generating the ontology from the Mediator database
     * (<i>compounds_db</i>) into the file <i>output/compound.pl</i> as well as
     * generating the JSON file associating each CMM identifier with the name of the
     * compound. It is necessary that the user copies the result in the
     * <i>prolog</i> directory.
     */
    public void generateCompoundOntology()
    {
        try
        {
            System.out.println("Generating ontology..."); 
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/compounds_db", "root", "socovos");
            FileWriter fw = new FileWriter(new File("output/compound_ontology.pl"));
            FileWriter fwNamesInJSON = new FileWriter(new File("output/names.json"));

            fw.write("% Automatically generated from transformer.jar (see javadoc in transformer/html)\n\n");
            fw.write(":- consult('meta_ontology.pl').\n\n");
            fw.write(":- discontiguous(class/3).\n");
            fw.write(":- discontiguous(is_directly_defined_sub_class_of/4).\n");
            fw.write(":- discontiguous(has_double_bonds/4).\n");
            fw.write(":- discontiguous(has_number_of_carbons/4).\n");
            fw.write(":- discontiguous(type_of_lipid_1/4).\n");
            fw.write(":- discontiguous(type_of_lipid/4).\n\n");
            
            Set<String> classes = new HashSet<>();
            Set<String> compounds = new HashSet<>();
            Map<String, String> scmc = new HashMap<>();
            Map<String, String> mcct = new HashMap<>();
            Statement stmt = con.createStatement();
/*            ResultSet rs21 = stmt.executeQuery("select c.compound_id, category, main_class, sub_class, number_carbons, double_bonds from compounds c " + 
                    "inner join compounds_lm_classification clc on c.compound_id=clc.compound_id " +
                    "inner join compounds_lipids_classification clcc on c.compound_id=clcc.compound_id");  */

            ResultSet rs1 = stmt.executeQuery("select compound_id, category, main_class, sub_class from compounds_lm_classification");
            while (rs1.next()) {
                String compoundId = rs1.getString("compound_id");
                String compoundIdValue = "t_compound_" + compoundId; 
                String subClass = rs1.getString("sub_class").toLowerCase();
                String mainClass = rs1.getString("main_class").toLowerCase();
                String category = rs1.getString("category").toLowerCase();
                String prologFactSubClass = "is_directly_defined_sub_class_of(" + compoundIdValue + ", " + subClass + ", sure, given).";
                String prologFactTypeOfLipid1 = "type_of_lipid_1(" + compoundIdValue + ", " + subClass + ", sure, given).";
                classes.add(compoundIdValue);
                compounds.add(compoundIdValue);
                scmc.put(subClass, mainClass);
                mcct.put(mainClass, category);
                //System.out.println(prologFactSubClass+"\n");
                fw.write(prologFactSubClass+"\n");
                //System.out.println(prologFactTypeOfLipid1);
                fw.write(prologFactTypeOfLipid1+"\n"); 
            }

            scmc.forEach((subClass, mainClass) -> {
                try{
                    String prologFactScIsClass ="class("+ subClass + ", sure, given).";
                    String prologFactMcIsClass ="class("+ mainClass + ", sure, given).";
                    String prologFactScMc = "is_directly_defined_sub_class_of(" + subClass + ", " + mainClass + ", sure, given).";
                    //System.out.println(prologFactScIsClass + "\n");
                    fw.write(prologFactScIsClass + "\n");
                    //System.out.println(prologFactMcIsClass + "\n");
                    fw.write(prologFactMcIsClass + "\n");
                    //System.out.println(prologFactScMc);
                    fw.write(prologFactScMc+"\n");
                }catch(IOException ex)
                {
                    System.err.println(ex);
                }
            });
            
            mcct.forEach((mainClass, category) -> {
                try{
                    String prologFactCatIsClass =  "class("+ category + ", sure, given).";
                    String prologFactMcCt = "is_directly_defined_sub_class_of(" + mainClass + ", " + category + ", sure, given).";
                    //System.out.println(prologFactCatIsClass + "\n");
                    fw.write(prologFactCatIsClass + "\n");
                    //System.out.println(prologFactMcCt + "\n");
                    fw.write(prologFactMcCt+"\n");
                }catch(IOException ex)
                {
                    System.err.println(ex);
                }
            });

            ResultSet rs2 = stmt.executeQuery("select compound_id, lipid_type, number_carbons, double_bonds from compounds_lipids_classification"); 
            while (rs2.next()) {
                String compoundId = rs2.getString("compound_id");
                String compoundIdValue = "t_compound_" + compoundId; 
                String lipidType = rs2.getString("lipid_type").replace("(", "_openpar").replace(")", "_closedpar").replace("-", "_hyphen").toLowerCase();
                String nCarbons = rs2.getString("number_carbons");
                String dBonds = rs2.getString("double_bonds");
                String prologFactTypeOfLipid = "type_of_lipid(" + compoundIdValue + ", " + lipidType + ", sure, given).";
                String prologFactMcCt = "is_directly_defined_sub_class_of(" + compoundIdValue + ", " + lipidType + ", sure, given).";
                String prologFactNCarbons = "has_number_of_carbons(" + compoundIdValue + " ," + nCarbons + " , sure, given).";
                String prologFactDBonds = "has_double_bonds("+compoundIdValue + " ," + dBonds + " , sure, given).";
                classes.add(compoundIdValue);
                classes.add(lipidType);
                //System.out.println(prologFactTypeOfLipid );
                fw.write(prologFactTypeOfLipid +"\n"); 
                //System.out.println(prologFactMcCt);
                fw.write(prologFactMcCt+"\n"); 
                //System.out.println(prologFactNCarbons);
                fw.write(prologFactNCarbons+"\n");
                //System.out.println(prologFactDBonds);
                fw.write(prologFactDBonds+"\n");
            }

            classes.forEach((compoundClass)-> { // they are both 'basic' compounds and lipid types.
                try{
                    String prologFactCompoundIsClass = "class("+compoundClass + ", sure, given).";
                    //System.out.println(prologFactCompoundIsClass);
                    fw.write(prologFactCompoundIsClass +"\n");
                }catch(IOException ex) {
                    System.err.println(ex);
                }
            });
            Set<TheoreticalCompound> theoreticalCompoundSet = new HashSet<>();
            compounds.forEach((compound)-> { // they are both 'basic' compounds and lipid types.
                try{
                    String identifier = compound.substring(11);
                    ResultSet rs3 = stmt.executeQuery("select compound_id, compound_name from compounds where compound_id = " + identifier);
                    rs3.next();
                    String name = rs3.getString("compound_name");
                    String thNameFact = "has_name("+compound + ", " + "\""+ rs3.getString("compound_name") + "\", sure, given).";
                    //System.out.println(thNameFact);
                    fw.write(thNameFact+"\n");
                    TheoreticalCompound theoreticalCompound = new TheoreticalCompound(Integer.parseInt(identifier), name);
                    theoreticalCompoundSet.add(theoreticalCompound); 
                }catch(SQLException | IOException ex) {
                    System.err.println(ex);
                }
            });
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonTheoreticalCompoundSet = gson.toJson(theoreticalCompoundSet);
            fwNamesInJSON.write(jsonTheoreticalCompoundSet);
            fw.close();
            fwNamesInJSON.close();
            con.close();

        }catch(SQLException | IOException ex)
        {
            System.err.println(ex);
        }
    }
}
