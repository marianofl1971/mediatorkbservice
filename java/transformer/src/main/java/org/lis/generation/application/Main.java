package org.lis.generation.application;

/**
 * This class has the resposibility to start the program.
 */
public class Main 
{
    /**
     * The program starts with this method.
     */
    public static void main(String[] args)
    {
        Coordinator.execute(args);
    }
}
