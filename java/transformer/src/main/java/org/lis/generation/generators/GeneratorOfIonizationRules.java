package org.lis.generation.generators;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

/**
 * This class is the responsible for generating the ionization rules from a 
 * spreadsheet.
 */
public class GeneratorOfIonizationRules
{
    
    /**
     * This method allows generating the ionization rules from the spreadsheet
     * <i>resources/lipids_ionization_type_rules.ods</i> into the file 
     * <i>output/ionization_rules.pl</i>. It is necessary that the user copies
     * the result in the prolog directory. An empty cell means that every value
     * is possible.
    */
    public static void generate()
    {
        try
        {
        File outFile = new File("output/ionization_rules.pl");
        FileWriter fw = new FileWriter(outFile);
        String string = generateString();
        System.out.println(string);
        fw.write(string);
        fw.close(); 
        }catch(IOException e)
        {
            System.err.println(e);
        }
    }
    
    private static String generateString()
    {
        StringBuilder sb = new StringBuilder("% Automatically generated from transformer.jar (see javadoc in transformer/html)\n\n");
        try
        {
            File file = new File("resources/lipids_ionization_type_rules.ods");
            final Sheet sheet = SpreadSheet.createFromFile(file).getSheet(0);
            for(int i = 9; i <= 118; i++)
            {
                String[] modifiers = obtainModifiers(sheet.getCellAt("B"+i).getTextValue());
                String ionization = sheet.getCellAt("C"+ i).getTextValue();
                String category = sheet.getCellAt("D"+i).getTextValue();
                String mainClass = sheet.getCellAt("E"+i).getTextValue();
                String[] subClasses = obtainSubClasses(sheet.getCellAt("F"+i).getTextValue());
                String adductCell = sheet.getCellAt("G"+i).getTextValue();
                String adduct =  adductCell.isEmpty() ? "_Adduct" : adductCell;
                String numericalTrueValue = sheet.getCellAt("I"+i).getTextValue();
                String trueValue = numericalTrueValue.equalsIgnoreCase("1F") ? "very_likely" : 
                                    numericalTrueValue.equalsIgnoreCase("0.5F") ? "likely" : "very_unlikely";

                sb.append(generateRuleFromValues(modifiers, ionization, category, mainClass, subClasses, adduct, trueValue));
            }
        }catch(IOException e)
        {
            System.err.println();
        }

        return sb.toString();
    }

    private static String generateRuleFromValues(String[] modifiers, String ionization, String category, 
            String mainClass, String[] subClasses, String adduct, String trueValue)
    {
       if (modifiers.length == 1 && subClasses.length > 1)
                return "ionization_inference_rule(" + modifiers[0] + ", " + ionization + 
                        ", " + category.toLowerCase() + ", " +  mainClass.toLowerCase() +  ", SubClass" +
                        ", " + generateAdductString(adduct) + ", " + trueValue +") :- \n\t" + generateUnificationCheckingForEachValue("SubClass", subClasses) + ".\n\n";
       else if (modifiers.length > 1 && subClasses.length == 1) 
                return "ionization_inference_rule(Modifier, " + ionization + 
                        ", " + category.toLowerCase() + ", " +  mainClass.toLowerCase() +  ", " + subClasses[0] +
                        ", " + generateAdductString(adduct) + ", " + trueValue +") :- " + 
                        "\n\t"+ generateUnificationCheckingForEachValue("Modifier", modifiers) + ".\n\n";
       else if (modifiers.length == 1 && subClasses.length == 1) 
                return "ionization_inference_rule(" + modifiers[0] + "," + ionization + 
                        ", " + category.toLowerCase() + ", " +  mainClass.toLowerCase() +  ", " + subClasses[0] +
                        ", " +  generateAdductString(adduct) + ", " + trueValue + ").\n\n"; 
       else return "ionization_inference_rule(Modifier, " + ionization + 
                        ", " + category.toLowerCase() + ", " +  mainClass.toLowerCase() + ", SubClass" +  
                        ", " +  generateAdductString(adduct) + ", " + trueValue +") :- \n\t" + generateUnificationCheckingForEachValue("SubClass", subClasses) + "," +
                        "\n\t"+ generateUnificationCheckingForEachValue("Modifier", modifiers) + ".\n\n";
    }

    private static String generateAdductString(String adduct)
    {
        return adduct.equalsIgnoreCase("_Adduct") ? "_Adduct" : "'" + adduct + "'";
    }

    private static String[] obtainModifiers(String subClassesIn)
    {
        String[] empty = {"_Modifier"};
        return subClassesIn.equals("") ? empty : subClassesIn.replace("(", "").replace(")", "").split(",");
    }
    
    private static String[] obtainSubClasses(String subClassesIn)
    {
        String[] empty = {"_SubClass"};
        return subClassesIn.equals("") ? empty : subClassesIn.replace("(", "").replace(")", "").replace("'", "").toLowerCase().split(",");
    }

    private static String generateUnificationCheckingForEachValue(String prologVariable, String[] adducts)
    {
        StringBuilder sb = new StringBuilder("(");
        for(String adduct : adducts) sb.append(prologVariable + " = " + adduct + "; ");
        sb.append(")");

        return sb.toString().replace("; )", ")");
    } 
    
}
