package org.lis.generation.application;

import org.lis.generation.generators.CompoundOntologyGenerator;
import org.lis.generation.generators.GeneratorOfIonizationRules;
import org.lis.generation.generators.TestGenerator;

/**
 * This class is the responsible for invoking the different generators. 
 */
public class Coordinator 
{
    /**
     * This class allows invoking the different generators (write <i>java -jar transformer.jar</i> for help).
     */
    public static void execute(String[] args)
    {
        if ( args.length == 0 || args[0].equalsIgnoreCase("help"))
        {
            showHelp();
        }
        else if (args[0].equalsIgnoreCase("tests"))
        {
            System.out.println("Please, wait. This operation can take some minutes...");
            TestGenerator ig = new TestGenerator();
            ig.generate();
        }
        else if (args[0].equalsIgnoreCase("ontology"))
        {
            CompoundOntologyGenerator cog = new CompoundOntologyGenerator();
            cog.generateCompoundOntology();
        }
        else if (args[0].equalsIgnoreCase("ionization"))
        {
            GeneratorOfIonizationRules.generate();
        }else
        {
            showHelp();
        }
    }

    private static void showHelp()
    {
        System.out.println("\n\u001B[3m tests\u001B[0m  - a Prolog file with tests from a real experiment is generated from a spreadsheet (its path is hard coded).\n" +
                "        \u001B[1m BE CAREFUL\u001B[0m : the number of rows is hard coded!");
        System.out.println("\u001B[3m ontology\u001B[0m  - a Prolog ontology is generated from a database that stores a compound hierarchy.");
        System.out.println("\u001B[3m ionization\u001B[0m  - a Prolog knowledge base module with ionization rules is generated from a spreadsheet (its path is hard coded).");
        System.out.println("\n\u001B[1m DO NOT FORGET TO COPY THE RESULT IN THE DESTINATION DIRECTORY with ./copy_pl_files.sh.\u001B[0m \n");
    }

}
