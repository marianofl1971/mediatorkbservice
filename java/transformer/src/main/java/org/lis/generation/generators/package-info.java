/**
* This package contains the classes responsible for generating the compound ontology from a Mediator database, the ionization rules from a spreadsheet, and tests of the Prolog knowledge base from a spreadsheet with the data of an experiment. 
*/
package org.lis.generation.generators;
