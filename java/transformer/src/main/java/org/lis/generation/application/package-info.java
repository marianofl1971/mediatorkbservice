/**
* This package has the class Main, which has the responsibility to start the
* program as well as the class Cordinator, which invokes the different
* generators. 
*/
package org.lis.generation.application;
