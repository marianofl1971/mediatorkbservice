#The model supporting JSON inputs

The input in JSON is compliant with the [LIS
Ontology](https://w3id.org/def/lis). It matches the
[experiment](https://w3id.org/def/lis#Experiment) class.
