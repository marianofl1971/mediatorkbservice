mvn install:install-file \
   -Dfile=target/lis-domain-1.0-jar-with-dependencies.jar \
   -DgroupId=org.lis \
   -DartifactId=lis-domain\
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true
