package org.lis.domain;

/**
 * The possible modifiers are NH3, HCOO, CH3COO, HCOONH3, CH3COONH3
 */
public enum Modifier
{
    modifier_NH3, modifier_HCOO, modifier_CH3COO, modifier_HCOONH3, modifier_CH3COONH3
}
