package org.lis.domain;

import java.util.HashSet;
import java.util.Set;

import org.lis.domain.PutativeAnnotation;

/**
 * An experiment consists of an identifier, (possiblely) a modifier, an ionization mode and a set of putative annotations.
 */
public class Experiment{
    private int identifier;
    private Modifier hasModifier;
    private IonizationMode hasIonizationMode;
    private Set<PutativeAnnotation> hasPutativeAnnotation = new HashSet<>();

    /**
     * It obtains the identifier.
     * @return The identifier
     * /
    public int getIdentifier() {
        return identifier;
    }

    /**
     * It obtains the modifier.
     * @return the modifier
     */
    public Modifier getHasModifier() {
        return hasModifier;
    }

    /**
     * It obtains the ionization mode.
     * @return the ionization mode.
     */
    public IonizationMode getHasIonizationMode() {
        return hasIonizationMode;
    }

    /**
     * It obtains the set of putative annotations.
     * @return the set of putative annotations.
     */
    public Set<PutativeAnnotation> getHasPutativeAnnotation() {
        return hasPutativeAnnotation;
    }

    /**
     * It builds an experiment from its identifier, modifier, its ionization
     * mode and its set of putative annotations (if there is no modifier, the parameter
     * is <i>null</i>.
     */
    public Experiment(int identifier, Modifier hasModifier, IonizationMode hasIonizationMode, Set<PutativeAnnotation> hasPutativeAnnotation) {
        this.identifier = identifier;
        this.hasModifier = hasModifier;
        this.hasIonizationMode = hasIonizationMode;
        this.hasPutativeAnnotation = hasPutativeAnnotation;
    }

}
