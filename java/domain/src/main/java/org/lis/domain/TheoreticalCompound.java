package org.lis.domain;

/**
 * An object of this class represent a theoretical compound with an identifier and a name.
 */
public class TheoreticalCompound{
    private int identifier;
    private String name;

    /**
     * It builds a theoretical compound from its identifier and its name.
     * @param identifier The identifier.
     * @param name The name.
     */
    public TheoreticalCompound(int identifier, String name) {
        this.identifier = identifier;
        this.name = name;
    }

    /**
     * It obtains the identifier.
     * @return The identifier.
     */
    public int getIdentifier() {
        return identifier;
    }

    /**
     * It obtains the name.
     * @return The name.
     */
    public String getName() {
        return name;
    }
}
