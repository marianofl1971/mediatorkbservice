package org.lis.domain;

/**
 * The ionization modes are positive and negative.
 */
public enum IonizationMode{
    positive, negative;
}
