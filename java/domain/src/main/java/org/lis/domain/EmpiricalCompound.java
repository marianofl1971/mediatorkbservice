package org.lis.domain;

/**
 * An empirical compound is represented by its mass and its retention time.
 *
 */
public class EmpiricalCompound{
    private Double mass;
    private Double retentionTime;

/**
 * It builds an empirical compound from its mass and its retention time.
 * @param mass The mass.
 * @param retentionTime The retention time.
 */
    public EmpiricalCompound(Double mass, Double retentionTime) {
        this.mass = mass;
        this.retentionTime = retentionTime;
    }

    /**
     * It obtains the mass.
     * @return The mass.
     */
    public Double getMass() {
        return mass;
    }

    
    /**
     * It obtains the retention time.
     * @return The retention time.
     */
    public Double getRetentionTime() {
        return retentionTime;
    }

}
