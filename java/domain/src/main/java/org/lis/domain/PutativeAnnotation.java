package org.lis.domain;

/**
 * An object of this class represents an putative annotation that establishes that a particular empirical compound is a particular theoretical compound with an adduct.
 */
public class PutativeAnnotation{
    private int identifier;
    private EmpiricalCompound empiricalCompound;
    private TheoreticalCompound theoreticalCompound;
    private String adduct;

    /**
     * It builds an putative annotation from an identifier, an empirical compound and an theoretial compound.
     * @param identifier The identifier.
     * @param empiricalCompound The empirircal compound.
     * @param theoreticalCompound The theoretical compound.
     * @param adduct The adduct.
     */
    public PutativeAnnotation(int identifier, EmpiricalCompound empiricalCompound, TheoreticalCompound theoreticalCompound, String adduct) {
        this.identifier = identifier;
        this.empiricalCompound = empiricalCompound;
        this.theoreticalCompound = theoreticalCompound;
        this.adduct = adduct;
    }

    /**
     * It obtains the identifier of the putative annotation.
     * @return identifier The identifier.
     */
    public int getIdentifier() {
        return identifier;
    }

    /**
     * It obtains the empirical compound.
     * @return The empirical compound.
     */
    public EmpiricalCompound getEmpiricalCompound() {
        return empiricalCompound;
    }

    /**
     * It obtains the theoretical compound.
     * @return The theoretical compound.
     */
    public TheoreticalCompound getTheoreticalCompound() {
        return theoreticalCompound;
    }

    public String getAdduct() {
        return adduct;
    }
}
