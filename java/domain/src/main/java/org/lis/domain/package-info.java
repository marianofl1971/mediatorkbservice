/**
 * This package contains the classes that represent the input data flow (a set of putative annotations) and the out one (a set of identifications).
 */
package org.lis.domain;
