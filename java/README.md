The [transfomer](transformer/README.md) has the code to generate part of the
rules in Prolog as well as to generate tests for the Prolog program.

The [domain](README.md) has the classes that provide support to the generation
of JSON files that can be used as inputs to the Prolog API REST to check it.

