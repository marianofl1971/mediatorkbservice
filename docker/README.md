# How to carry out the dockerisation

It can be done through the following script execution

~~~~
./dockerise.sh
~~~~

# How to run the server

Executing Docker

~~~~
docker run -it -d -p 8081:8080 las
~~~~
