%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                NUMERICAL VALUES OF TRUE VALUES                   %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numerical_value(not_applicable, 0).
numerical_value(very_unlikely, 1).
numerical_value(unlikely, 2).
numerical_value(hypothesised, 3).
numerical_value(likely, 4).
numerical_value(very_likely, 5).
numerical_value(sure, 6).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                 AGGREGATION OF TRUE VALUES                       %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The aggregated true value is the weakest one in the antecedent. 

% sort/2 obtains the sorted list without replicated elements.
aggregate_true_values_by_default(TrueValueList, TrueValue) :-
    true_values_into_numerical_values(TrueValueList, NumericalValueList),
    sort(NumericalValueList, [NumericalValue | _R]),
    numerical_value(TrueValue, NumericalValue),
    TrueValue \= very_unlikely, TrueValue \= not_applicable, !.

% if some true value is 'very_unlikely' or 'not_applicable' => the aggregated true value is 'not_applicable'.

aggregate_true_values_by_default(TrueValueList, not_applicable) :- 
    true_values_into_numerical_values(TrueValueList, NumericalValueList),
    sort(NumericalValueList, [NumericalValue | _R]),
    numerical_value(TrueValue, NumericalValue),
    (TrueValue = very_unlikely; TrueValue = not_applicable), !.

% true values into numerical values.

true_values_into_numerical_values([], []).

true_values_into_numerical_values([TrueValue | R], [NumericalValue | RN]) :- 
    numerical_value(TrueValue, NumericalValue),
    true_values_into_numerical_values(R, RN).
