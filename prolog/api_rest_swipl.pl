% Inspired from http://www.swi-prolog.org/howto/http/HelloText.html
% and http://www.swi-prolog.org/pldoc/man?section=http-read-post

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_client)).
:- use_module(library(http/json)).

% This is the starting handler, the rest are only for evaluation tasks.
:- http_handler(root(process_experiment), reply_process_experiment, []).

:- http_handler(root(load_experiment), reply_load_experiment, []).
:- http_handler(root(clean), reply_clean, []).

% Domain consult

:-consult('english.pl').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This clause base is structured as follows:
%
% 1. Predicate to start the server.
% 2. Predicate to run an experiment from a POST request. 
% 3. Predicates to load an experiment from the POST request.
% 4. Predicates to invoke rules with explanations.
% 5. Predicates to clean the case-dependent data.
% 6. Auxiliar predicates.
% 
% IMPORTANT: see reply_process_experiment/1 to understand the
% execution sequence.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 1. Predicate to start the server
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2. Predicate to process a whole experiment from a POST request
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reply_process_experiment(Request) :-
    load_experiment(Request),
    fire_rules(Lj),
    clean_experiment,
    format('Content-type: application/json~n~n', []),
    json_write_dict(current_output, Lj).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 3. Predicates to load the experiment from the POST request
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reply_load_experiment(Request) :-
    load_experiment(Request),
    format('Content-type: application/json~n~n', []),
    json_write(current_output, experiment_asserted{result:ok}).

load_experiment(Request) :-
    member(method(post), Request), !,
    http_read_data(Request, Data, []),
    atom_json_term(Data, json(Experiment), []),
    assert_modifier(Experiment),
    assert_ionization(Experiment),
    process_putative_annotations(Experiment).

modifier_code(modifier_NH3, 1).
modifier_code(modifier_HCOO, 2).
modifier_code(modifier_CH3COO, 3).
modifier_code(modifier_HCOONH3, 4).
modifier_code(modifier_CH3COONH3, 5).

ionization_code(positive, 1).
ionization_code(negative, 2).

assert_modifier(L):-
    member(hasModifier=Modifier, L), 
    modifier_code(Modifier, ModifierCode),
    assert(modifier(ModifierCode, sure)).

assert_modifier(L):-
    \+ member(hasModifier=_Modifier, L), 
    assert(modifier(none, sure)).

assert_ionization(L):-
    member(hasIonizationMode=IM, L),
    ionization_code(IM, IC),
    assert(ionization(IC, sure)). 

process_putative_annotations(L):-
    member(hasPutativeAnnotation=JsonWithPutativeAnnotations, L),
    assert_putative_annotations(JsonWithPutativeAnnotations).

assert_putative_annotations([]).

assert_putative_annotations([json(H) | R]) :-
    assert_putative_annotation(H),
    assert_putative_annotations(R).

assert_putative_annotation(H):-
    member(identifier=HypId, H),
    member(empiricalCompound=json(EmpiricalCompound), H),
    member(mass=Mass, EmpiricalCompound),
    member(retentionTime=RT, EmpiricalCompound),
    member(theoreticalCompound=json(TheoreticalCompound), H),
    member(identifier=ThId, TheoreticalCompound),
    member(name=ThName, TheoreticalCompound),
    member(adduct=Adduct, H),
    concat("t_compound_", ThId, DirectClassString),
    term_string(DirectClass, DirectClassString),
    assert(is_directly_defined_instance_of(e_compound(Mass, RT), DirectClass, hypothesised, hypothesis(HypId))),
    assert(has_adduct(e_compound(Mass, RT), Adduct, hypothesised, hypothesis(HypId))),
    assert(has_name(DirectClass, ThName, sure, given)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 4. Predicates to invoke rules with explanations
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fire_rules(Lj) :-
    ionization_rules_with_just_core_info(Lj1),
    ionization_rules_with_core_and_additional_info(Lj2),
    adduct_relation_rules(Lj3),
    retention_time_rules_just_for(Lj4),
    retention_time_rules_just_against(Lj5),
    retention_time_rules_for_against(Lj6),
    append(Lj1, Lj2, Lj12),
    append(Lj12, Lj3, Lj123),
    append(Lj123, Lj4, Lj1234),
    append(Lj1234, Lj5, Lj12345),
    append(Lj12345, Lj6, Lj).

ionization_rules_with_just_core_info(Lj) :-
    findall((ionization_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, no_addInfo, no_for, no_against), 
    (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, ionization_rule_explanation(core_information(CI))), 
        has_name(TheoreticalCompound, Name, sure, given),
        has_th_identifier(TheoreticalCompound, ThId)), L),
        without_repeated_clae(L, Lwr),
        rule_in_json(Lwr, Lj).

ionization_rules_with_core_and_additional_info(Lj) :-
        findall((ionization_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, AE, no_for, no_against), 
        (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, ionization_rule_explanation(core_information(CI), additional_evidence(AE))), 
            has_name(TheoreticalCompound, Name, sure, given),
            has_th_identifier(TheoreticalCompound, ThId)), L),
            without_repeated_clae(L, Lwr),
            rule_in_json(Lwr, Lj).

adduct_relation_rules(Lj) :-
        findall((adduct_relation_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, no_addInfo, no_for, no_against), 
        (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, adduct_relation_rule_explanation(core_information(CI))), 
            has_name(TheoreticalCompound, Name, sure, given),
            has_th_identifier(TheoreticalCompound, ThId)), L),
            without_repeated_clae(L, Lwr),
            rule_in_json(Lwr, Lj).

retention_time_rules_just_for(Lj) :-
        findall((retention_time_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, no_addInfo, Efor, no_against), 
        (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, retention_time_rule(core_information(CI), evidences_for(Efor))), 
            has_name(TheoreticalCompound, Name, sure, given),
            has_th_identifier(TheoreticalCompound, ThId)), L),
            without_repeated_clae(L, Lwr),
            rule_in_json(Lwr, Lj).

retention_time_rules_just_against(Lj) :-
        findall((retention_time_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, no_addInfo, no_for, Eagainst), 
        (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, retention_time_rule(core_information(CI), evidences_against(Eagainst))), 
            has_name(TheoreticalCompound, Name, sure, given),
            has_th_identifier(TheoreticalCompound, ThId)), L),
            without_repeated_clae(L, Lwr),
            rule_in_json(Lwr, Lj).

retention_time_rules_for_against(Lj) :-
        findall((retention_time_rule_type, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, no_addInfo, Efor, Eagainst), 
        (has_identity_explained_in_English(N, e_compound(Mass, RT), Adduct, TheoreticalCompound, TrueValue, retention_time_rule(core_information(CI), evidences_for(Efor), evidences_against(Eagainst))), 
            has_name(TheoreticalCompound, Name, sure, given),
            has_th_identifier(TheoreticalCompound, ThId)), L),
            without_repeated_clae(L, Lwr),
            rule_in_json(Lwr, Lj).

has_th_identifier(TheoreticalCompound, Id) :-
    term_string(TheoreticalCompound, ThString),
    sub_string(ThString, 0, Length, X, t_compound_),
    sub_string(ThString, Length, X, _Y, IdString),
    term_string(Id, IdString).

json_explanation_pattern(CI, no_addInfo, no_for, no_against, explanation=json([coreInformation=CI])).

json_explanation_pattern(CI, AE, no_for, no_against, explanation=json([coreInformation=CI, additionalInformation=AE])):-
    AE \= no_addInfo.

json_explanation_pattern(CI, no_addInfo, EF, no_against, explanation=json([coreInformation=CI, evidenceFor=EF])):-
    EF \= no_for.

json_explanation_pattern(CI, no_addInfo, no_for, EA, explanation=json([coreInformation=CI, evidenceAgainst=EA])):-
    EA \= no_against.

json_explanation_pattern(CI, no_addInfo, EF, EA, explanation=json([coreInformation=CI, evidenceFor=EF, evidenceAgainst=EA])):-
    EF \= no_for,
    EA \= no_against.

rule_in_json([], []).

rule_in_json([(RuleType, N, Mass, RT, Adduct, ThId, Name, TrueValue, CI, AE, EF, EA) | R], 
    [
       json(
              [
                 ruleType=RuleType, 
                 trueValue=TrueValue, 
                 putativeAnnotation=json(
                                   [
                                      identifier=N, 
                                      empiricalCompound=json([mass=Mass, rt=RT]),
                                      theoreticalCompound=json([identifier=ThId, name=Name]), 
                                      adduct=Adduct
                                   ]
                                 ),
                 Explanation
             ]
          ) 
       | Rj]) :-
             json_explanation_pattern(CI, AE, EF, EA, Explanation),
             rule_in_json(R, Rj).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 5. Predicates to clean the case-dependent data. 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reply_clean(_Request) :-
    clean_experiment,
    format('Content-type: application/json~n~n', []),
    json_write(current_output, experiment_cleaned{result:ok}).

clean_experiment :-
    retractall(is_directly_defined_instance_of(e_compound(_Mass1, _RT1), _DirectClass1, hypothesised, hypothesis(_HypId1))),
    retractall(has_adduct(e_compound(_Mass2, _RT2), _Adduct2, hypothesised, hypothesis(_HypId2))),
    retractall(has_name(_DirectClass3, _ThName3, sure, given)),
    retractall(ionization(_IC4, sure)), 
    retractall(modifier(_Modifier5, sure)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 6. Auxiliary predicates 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This predicate is thought to avoid the problems that Prolog raises with the strings.
% Two certainty level assignments with explanations are equals if and only if their rule type and their 
% putative annotation identifier are the same
equals((RuleType, N, _Mass1, _RT1, _Adduct1, _ThId1, _Name1, _TrueValue1, _CI1, _AE1, _EF1, _EA1),
(RuleType, N, _Mass2, _RT2, _Adduct2, _ThId2, _Name2, _TrueValue2, _CI2, _AE2, _EF2, _EA2)).

member_clae(MC1, [MC2]) :- equals(MC1, MC2).
member_clae(MC1, [MC2 | _R]) :- equals(MC1, MC2). 
member_clae(MC1, [MC2 | R]) :- \+ equals(MC1, MC2), member_clae(MC1, R). 

member_clae((RuleType, N, _Mass1, _RT1, _Adduct1, _ThId1, _Name1, _TrueValue1, _CI1, _AE1, _EF1, _EA1), 
[(RuleType, N, _Mass2, _RT2, _Adduct2, _ThId2, _Name2, _TrueValue2, _CI2, _AE2, _EF2, _EA2)]).

without_repeated_clae(L1, L2):-
    without_repeated_clae(L1, L2, []).

without_repeated_clae([],[],_L).

without_repeated_clae([X | R1], [X | R2], Lformer) :-
    \+ member_clae(X, Lformer),
    without_repeated_clae(R1, R2, [X | Lformer]),!.

without_repeated_clae([X | R1], R2, Lformer) :-
    member_clae(X, Lformer),
    without_repeated_clae(R1, R2, [X | Lformer]),!.
