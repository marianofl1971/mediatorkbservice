:-consult('compound_ontology.pl').
:-consult('ionization_rules.pl').

:- discontiguous(has_adduct/4).
:- discontiguous(is_directly_defined_instance_of/4).
:- discontiguous(ionization/2).
:- discontiguous(modifier/2).
:- discontiguous(has_identity/5).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%         Clause base                                              %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This clause base is structured as follows: 
%
% 1. Implementation of ionization rules.
% 2. Implementation of adduct relation rules.
% 3. Implementation of retention time rules.
% 
% Along this document the terminological clarifications presented below
% should be taken into account:
%
% - The term 'rule' is used to refer to a heuristic acquired from the experts.
%   A rule is formalised in Prolog using clauses.
%
% - 'Hypothesis' and 'putative identification' will be considered synonyms. Each
%   hypothesis will have assigned a number that identifies it. Thus, for example,
%   the hypothesis '399,3367@18,8425 is the compound 32675 with adduct M+H' may
%   be identified with the number 3; the hypothesis '399,3367@18,8425 is the
%   compound 32694 with adduct M+H', with the number 4; etc.
%
% - The meaning of the 't_compound_0' constant symbol is 'no theoretical compound
%   has been found for the empirical compound'.
%
% To obtain all the hypothesis to which some rule is applicable, the query
% (henceforth query 2) is
%
% ?- has_identity(N, EmpiricalCompound, TheoreticalCompound, ThName, Adduct, TrueValue, Explanation).
%
% which means 'I want to know all the hypothesis that identify an empirical
% compound as a theoretical compound'. The answer will include, for each
% hypothesis, its true value (very_unlikely, likely or very_likely) and its
% explanation. N is used for the number of hypothesis. ThName is the name
% of the TheoreticalCompound.

has_identity(N, EmpiricalCompound, TheoreticalCompound, ThName, Adduct, TrueValue, Explanation) :-
    has_identity(N, EmpiricalCompound, TheoreticalCompound, TrueValue, Explanation),
    has_name(TheoreticalCompound, ThName, sure, given),
    has_adduct(EmpiricalCompound, Adduct, hypothesised, hypothesis(N)).

% 1. IMPLEMENTATION OF IONIZATION RULES
%
% These rules establishe that, for each hypothesis, it should be checked
% whether the theoretical compound belongs to a particular sub-hierarchy of
% lipids. Depending on the ionization state of the experiment, the result of a
% rule may be that the hypothesis is very_likely, likely or very_unlikely. If the
% hypothesis is also supported by some adduct relation rule (see 2. IMPLEMENTATION
% OF ADDUCT RELATION RULES), the score of the ionization rule increases to
% 'very_likely'.
%
% Each ionization rule has been represented as a clause using the predicate
% is_directly_defined_instance_of/4 (see file ionization_rules.pl). The clause 
% defined in the present file checks if some hypothesis satisfies some of the
% ionization rules. 
%
% The application of this clause can be seen with the following example. Let us
% suppose that hypothesis 81 assumes the compound 421.3169@18.8425 is
% identified as 6692. Then an answer to the following query (query 2):
% 
%    has_identity(N, EmpiricalCompound, TheoreticalCompound, TrueValue, Explanation).
%
% is 
% 
% N = 81,
% EmpiricalCompound = e_compound(421.3169, 18.8425),
% TheoreticalCompound = t_compound_6692,
% TrueValue = very_unlikely,
% Explanation = ionization_rule([none, 1, gp, gp04, gp0403, 'M+2H'])
%
% which means that hypothesis 81 establishes that
%
% 1. the empirical compound 421.3169@18.8425 is identified as the theoretical
%    compound 6692; 
% 2. such an identification is very_likely; and
% 3. the explanation is that an ionization rule has been applied with the
%    following facts:
%    a. there is no modifier;
%    b. the ionization state along the experiment is 1 (positive);
%    c. the compound belongs to the GP category, the GP04 main class and the
%       GP0403 sub-class; and d. the adduct is M+2H.
%
% Let us note that, first of all, the clause defining the
% predicate has_identity_by_ionization/5 is called. Paying attention to the
% tail of the clause, it can be observed that the putative identification of
% the compound is provided by hypothesis.  The sub-class, the main class and
% the category of the theoretical compound (e.g. 6692) are given by the
% ontology of compounds (which has been automatically generated from a
% database).
%
% Once this clause has been executed, it is checked if the theoretical compound
% is supperted by a adduct relation rule. In such a case, the true value is
% increased to 'very_likely', independently of the result of the evaluation of
% the predicate has_identity_by_ionization/5.

has_identity(N, EmpiricalCompound, TheoreticalCompound, TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct])) :-
    has_identity_by_ionization(N, EmpiricalCompound, TheoreticalCompound, TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct])),
    \+ has_identity(_N1, _EmpiricalCompound1, TheoreticalCompound, very_likely, adduct_relation_rule(_L)).

has_identity(N, EmpiricalCompound, TheoreticalCompound, very_likely, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct, adduct_relation_rules([X | R])])) :-
    has_identity_by_ionization(N, EmpiricalCompound, TheoreticalCompound, _TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct])),
    findall(E, has_identity(_N1, _EmpiricalCompound1, TheoreticalCompound, very_likely, adduct_relation_rule(E)), [X | R]).

has_identity_by_ionization(N, EmpiricalCompound, TheoreticalCompound, TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct])) :-
    is_directly_defined_instance_of(EmpiricalCompound, TheoreticalCompound, hypothesised, hypothesis(N)), 
    modifier(Modifier, sure), 
    ionization(Ionization, sure),
    has_adduct(EmpiricalCompound, Adduct, hypothesised, hypothesis(N)),
    is_directly_defined_sub_class_of(TheoreticalCompound, SubClass, sure, given),
    is_directly_defined_sub_class_of(SubClass, MainClass, sure, given),
    is_directly_defined_sub_class_of(MainClass, Category, sure, given),
    ionization_inference_rule(Modifier, Ionization, Category, MainClass, SubClass, Adduct, TrueValue),
    TheoreticalCompound \= t_compound_0.

% 2. IMPLEMENTATION OF ADDUCT RELATION RULES

% These rules check whether there are pairs of compounds putatively identified
% as the same theoretical compound, and with adducts satisfying some of the
% following conditions:
% 
% 1. The adduct of the first compound is M+H and the adduct of the second one
%    is M+Na, or vice versa.
% 2. The adduct of the first compound is M+H and the adduct of the second one 
%    is M+K, or vice versa.
% 3. The adduct of the first compound is M+Na and the adduct of the second one 
%    is M+K, or vice versa.
%
% Two possibilities are considered:
%
% Possibility 1. A scenario with three compounds 1, 2 and 3 identified as the
% same theoretical compound. The pairs compound 1 and compound 2 as well as
% compound 2 and compound 3 satisfy the conditions of relations between
% adducts. To detect this situation, the predicate
% has_identity_of_triple_scenario/5 is evaluated.
%
% Possiblity 2. A scenario with two compounds: compound 1 and compund 2 (both
% of them identified as the same theoretical compound) satisfy the conditions
% of relations between adducts. To detect this situation, the predicate
% has_identity_of_double_scenario/5 is evaluated. The scenario with three
% compounds provides more evidence for the identifications. Consequently, the
% scenario with two compounds is checked only if the pair is not involved in a
% scenario with three compounds.
%
% The predicate that formalises the relation between adducts is
% relations_between_adducts/4.
%
% Assuming hypothesis 3 saying 399,3367@18,8425 is the compound 32675 with
% adduct M+H, an answer for query 2 is
%
% N = 3,
% EmpiricalCompound = e_compound(399.3367, 18.8425),
% TheoreticalCompound = t_compound(32675),
% TrueValue = very_likely,
% Explanation = adduct_relation_rule([t_compound(32675), 
%                                     e_compound(399.3367, 18.8425), 'M+H',
%                                     e_compound(421.3169, 18.8425), 'M+Na'])
%
% The following facts have been taken into account in arriving at the answer:
%
% 1. The empirical compound 399.3367@18.8425 is tentatively identified as
%    compound 32675 with adduct M+H.
% 2. Similarly, the empirical compound
%    421.3169@18.8425 is putatively identified as compound 32675 with adduct
%    M+Na.
% 3. M+H and M+Na are adducts related through relations_between_adducts/4.
%
% The explanation uses the functor adduct_relation_rule/1 and a list with the
% relevant information to understand why the rule has been applied. Such information
% includes the empirical compounds involved in the rule with their adducts as
% well as the theoretical compounds assigned in the putative identification.


has_identity(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, Explanation) :-
    has_identity_of_triple_scenario(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, Explanation).

has_identity(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, Explanation) :-
    \+has_identity_of_triple_scenario(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, _ExplanationTS),
    has_identity_of_double_scenario(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, Explanation).

has_identity_of_triple_scenario(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, adduct_relation_rule([TheoreticalCompound, EmpiricalCompound1, Adduct1, EmpiricalCompound2, Adduct2, EmpiricalCompound3, Adduct3])) :-
    %The trick so that this rule is efficient is to start by the most restricted 
    %conditions: relations between adducts
    relations_between_adducts(Adduct1, Adduct2, sure, given), 
    relations_between_adducts(Adduct1, Adduct3, sure, given),
    Adduct2 \= Adduct3,
    %scenario 1
    is_directly_defined_instance_of(EmpiricalCompound1, TheoreticalCompound, hypothesised, hypothesis(N1)),
    TheoreticalCompound \= t_compound_0,
    has_adduct(EmpiricalCompound1, Adduct1, hypothesised, hypothesis(N1)),
    %scenario 2
    is_directly_defined_instance_of(EmpiricalCompound2, TheoreticalCompound, hypothesised, hypothesis(N2)),
    TheoreticalCompound \= t_compound_0,
    has_adduct(EmpiricalCompound2, Adduct2, hypothesised, hypothesis(N2)),
    EmpiricalCompound2 \= EmpiricalCompound1,
    %scenario 3
    is_directly_defined_instance_of(EmpiricalCompound3, TheoreticalCompound, hypothesised, hypothesis(N3)),
    TheoreticalCompound \= t_compound_0,
    has_adduct(EmpiricalCompound3, Adduct3, hypothesised, hypothesis(N3)),
    EmpiricalCompound3 \= EmpiricalCompound1,
    EmpiricalCompound3 \= EmpiricalCompound2.

has_identity_of_double_scenario(N1, EmpiricalCompound1, TheoreticalCompound, very_likely, adduct_relation_rule([TheoreticalCompound, EmpiricalCompound1, Adduct1, EmpiricalCompound2, Adduct2])) :-
    relations_between_adducts(Adduct1, Adduct2, sure, given),
    %scenario 1
    is_directly_defined_instance_of(EmpiricalCompound1, TheoreticalCompound, hypothesised, hypothesis(N1)),
    TheoreticalCompound \= t_compound_0,
    has_adduct(EmpiricalCompound1, Adduct1, hypothesised, hypothesis(N1)),
    %scenario 2
    is_directly_defined_instance_of(EmpiricalCompound2, TheoreticalCompound, hypothesised, hypothesis(N2)),
    TheoreticalCompound \= t_compound_0,
    has_adduct(EmpiricalCompound2, Adduct2, hypothesised, hypothesis(N2)),
    EmpiricalCompound2 \= EmpiricalCompound1.

relations_between_adducts_d('M+H', 'M+Na', sure, given).
relations_between_adducts_d('M+H', 'M+K', sure, given).
relations_between_adducts_d('M+Na', 'M+K', sure, given).

relations_between_adducts(Adduct1, Adduct2, sure, given) :-
    relations_between_adducts_d(Adduct1, Adduct2, sure, given);
    relations_between_adducts_d(Adduct2, Adduct1, sure, given).


% 3. IMPLEMENTATION OF RETENTION TIME RULES
%
% Let us suppose that a hypothesis N1 assumes that the empirical compound
% Mass1@RT1 is identified as the theoretical compound 1. Let's also suppose
% that another hypothesis (N2) assumes that the empirical compound Mass2@RT2 is
% identified as the theoretical compound 2. Let's suppose that compounds 1 and
% 2 are lipids of the same type. Then the following facts are, according to the
% retention time rules, evidences suporting the hypothesis:
%
% 1. Compounds 1 and 2 have the same number of carbons, compound 1 has more
%    double bonds and RT1 < RT2.
% 2. Compounds 1 and 2 have the same number of double bonds, compound 1 has
%    less number of carbons and RT1 < RT2.  
%
% These evidences supporting the hypothesis N1 may coexit with evidences
% against. Thus, if either fact 1 or 2 happens, the hypothesis is 'very_likely'
% unless one of the following facts happen:
%
% 1. Compounds 1 and 2 have the same number of carbons, compound 1 has more
%    double bonds and RT1 >= RT2.
% 2. Compounds 1 and 2 have the same number of double bonds, compound 1 has
%    less number of carbons and RT1 >= RT2.  
% 
% The predicate used to search for evidence supporting the hypothesis is
% for_retention_time_rule/4, and the predicate used to search for evidence
% against is against_retention_time_rule/4. The evidence is 'very_likely' if there
% is evidence supporting the hypothesis, but not against. It is 'likely' if
% there are evidences both supporting and against the hypothesis.
% If just evidence against exists, then the true value is 'unlikely'.
%
% A likely answer to query 2 is one shown below:
%
% N = 3,
% EmpiricalCompound = e_compound(421.3169, 18.8425),
% TheoreticalCompound = t_compound_32675,
% TrueValue = very_likely,
% LipidType = car_openpar,
% NCarbons1 = 16,
% NDoubleBonds1 = 0,
% Lfor = [ (96, e_compound(315.2424, 8.1449), t_compound_32655, 10, 0), (98, e_compound(315.2424, 8.1449), t_compound_32602, 10, 0), (288, e_compound(287.2104, 4.0216), t_compound_32598, 8, 0), (290, e_compound(287.2104, 4.0216), t_compound_32691, 8, 0), (893, e_compound(425.3504, 19.5463), t_compound_32639, 18, 0)]
%
% The explanation means that the empirical compound 421.3169@18.8425 is
% identified as the theoretical compound 32675, a 'CAR(' lipid with 16 carbons
% and 0 double bonds; and that this is compatible with the identification of, e.g.,
% 315.2424@8.1449 as 32655, which is also a 'CAR(' lipid with 10 carbons and
% no double bond. Let us note that the retention time of the second empirical
% compound is lesser than the retention time of the first one.
%
% If there were evidence against the hypothesis, the list of hypothesis involved
% in such evidence would appear as well. 

has_identity(N1, e_compound(Mass1, RT1), TheoreticalCompound1, TrueValue, retention_time_rule([e_compound(Mass1, RT1), TheoreticalCompound1, LipidType, NCarbons1, NDoubleBonds1, Lfor, Lagainst])):-
    is_directly_defined_instance_of(e_compound(Mass1, RT1), TheoreticalCompound1, hypothesised, hypothesis(N1)),
    type_of_lipid(TheoreticalCompound1, LipidType, sure, given),
    has_number_of_carbons(TheoreticalCompound1, NCarbons1, sure, given),
    has_double_bonds(TheoreticalCompound1, NDoubleBonds1, sure, given),
    findall((N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds2), 
    for_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons1, NDoubleBonds1, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds2), Lfor),
    findall((N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds2), 
    against_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons1, NDoubleBonds1, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds2), Lagainst),
    true_value_rt(Lfor, Lagainst, TrueValue).

true_value_rt([], [_Xaginst | _Ragainst], unlikely).
true_value_rt([_Xfor | _Rfor], [_Xagainst | _Ragainst], likely).
true_value_rt([_Xfor | _Rfor], [], very_likely).

for_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons, NDoubleBonds1, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons, NDoubleBonds2):-
    is_directly_defined_instance_of(e_compound(Mass2, RT2), TheoreticalCompound2, hypothesised, hypothesis(N2)),
    e_compound(Mass1, RT1) \= e_compound(Mass2, RT2),
    type_of_lipid(TheoreticalCompound2, LipidType, sure, given),
    has_number_of_carbons(TheoreticalCompound2, NCarbons, sure, given),
    has_double_bonds(TheoreticalCompound2, NDoubleBonds2, sure, given),
    (NDoubleBonds1 > NDoubleBonds2, RT1 < RT2; NDoubleBonds1 < NDoubleBonds2, RT1 > RT2).

for_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons1, NDoubleBonds, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds):-
    is_directly_defined_instance_of(e_compound(Mass2, RT2), TheoreticalCompound2, hypothesised, hypothesis(N2)),
    e_compound(Mass1, RT1) \= e_compound(Mass2, RT2),
    type_of_lipid(TheoreticalCompound2, LipidType, sure, given), 
    has_number_of_carbons(TheoreticalCompound2, NCarbons2, sure, given),
    has_double_bonds(TheoreticalCompound2, NDoubleBonds, sure, given),
    (NCarbons1 < NCarbons2, RT1 < RT2; NCarbons1 > NCarbons2, RT1 > RT2).

against_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons, NDoubleBonds1, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons, NDoubleBonds2):-
    is_directly_defined_instance_of(e_compound(Mass2, RT2), TheoreticalCompound2, hypothesised, hypothesis(N2)),
    e_compound(Mass1, RT1) \= e_compound(Mass2, RT2),
    type_of_lipid(TheoreticalCompound2, LipidType, sure, given),   
    has_number_of_carbons(TheoreticalCompound2, NCarbons, sure, given),
    has_double_bonds(TheoreticalCompound2, NDoubleBonds2, sure, given),
    (NDoubleBonds1 > NDoubleBonds2, RT1 >= RT2; NDoubleBonds1 < NDoubleBonds2, RT1 =< RT2).

against_retention_time_rule(LipidType, e_compound(Mass1, RT1), NCarbons1, NDoubleBonds, N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds):-
    is_directly_defined_instance_of(e_compound(Mass2, RT2), TheoreticalCompound2, hypothesised, hypothesis(N2)),
    e_compound(Mass1, RT1) \= e_compound(Mass2, RT2),
    type_of_lipid(TheoreticalCompound2, LipidType, sure, given),   
    has_number_of_carbons(TheoreticalCompound2, NCarbons2, sure, given),
    has_double_bonds(TheoreticalCompound2, NDoubleBonds, sure, given),
    (NCarbons1 < NCarbons2, RT1 >= RT2; NCarbons1 > NCarbons2, RT1 =< RT2).
