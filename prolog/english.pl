:-consult('kb.pl').

:- discontiguous(has_identity_explained_in_English/6).
:- discontiguous(explanation_in_English/3).
:- discontiguous(explanation_in_English/5).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                          %
%         Clause base for explanations                     %
%                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lists with the keys for explanations are converted into
% English text. These clause base is decoupled from the
% identification clause base so that the system can be
% extended to provide explanations in other natural
% languages.
% 
% 'has_identity_explained_in_English/6' is the predicate 
% that should be invoked to obtain identifications
% with explanations in English.
% 
% 'explanation_in_English' is a set of  predicates with 
% different arities that associate results and explanations
% in compound identification with explanations in English.
%
% The predicate concat(+List, ?Explanation) has been
% defined in this clause base to concatenate a list
% of strings.
%
% The sentences are adapted to different data so that
% the are natural for the human reader. Thus, for
% example, it is distinguished between 'no double bond',
% 'one double bond', and the rest of cases, e.g.,
% '2 double bonds'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                          %
%   EXPLANATIONS FOR IONIZATION RULES                      %
%                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

has_identity_explained_in_English(N, EmpiricalCompound, Adduct, TheoreticalCompound, TrueValue, ExplanationInEnglish) :- 
    has_identity(N, EmpiricalCompound, TheoreticalCompound, ThName, Adduct, TrueValue, ionization_rule(E)),
    explanation_in_English(ThName, EmpiricalCompound, TrueValue, ionization_rule(E), ExplanationInEnglish).

explanation_in_English(ThName, e_compound(Mass, RT), TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct]), ionization_rule_explanation(core_information(CoreExplanationInEnglish))) :- 
    ionization_in_English(Ionization, IonizationInEnglish),
    modifier_in_English(Modifier, ModifierInEnglish),
    string_upper(Category, CategoryU),
    string_upper(SubClass, SubClassU),
    string_upper(MainClass, MainClassU),
    true_value_in_English(TrueValue, TrueValueInEnglish),
    concat(["It is ", TrueValueInEnglish, " that the empirical compound ", Mass, "@", RT, " (in an experiment with ", IonizationInEnglish, " ionization and ", ModifierInEnglish, ") is the theoretical compound ", ThName, " with adduct ", Adduct, ", which belongs to the hierarchy ", CategoryU, ", ", MainClassU, ", ", SubClassU], CoreExplanationInEnglish).

% This option can be checked with the hypothesis 296 of the test set.
explanation_in_English(ThName, e_compound(Mass, RT), _TrueValue, ionization_rule([Modifier, Ionization, Category, MainClass, SubClass, Adduct, adduct_relation_rules(L)]), ionization_rule_explanation(core_information(CoreExplanationInEnglish), additional_evidence(ExplanationInEnglishARR))) :-
    ionization_in_English(Ionization, IonizationInEnglish),
    modifier_in_English(Modifier, ModifierInEnglish),
    string_upper(Category, CategoryU),
    string_upper(SubClass, SubClassU),
    string_upper(MainClass, MainClassU),
    map_explanation_list_to_English(ThName, L, [ExplanationInEnglishARR|_R]),
    concat(["It is very likely that the empirical compound ", Mass, "@", RT, " (in an experiment with ", IonizationInEnglish, " ionization and ", ModifierInEnglish, ") is the theoretical compound ", ThName, " with adduct ", Adduct, ", which belongs to the hierarchy ", CategoryU, ", ", MainClassU, ", ", SubClassU], CoreExplanationInEnglish).

true_value_in_English(very_unlikely, "very unlikely").
true_value_in_English(unlikely, "unlikely").
true_value_in_English(likely, "likely").
true_value_in_English(very_likely, "very likely").

ionization_in_English(1, "positive").
ionization_in_English(2, "negative").
modifier_in_English(none, "no modifier").
modifier_in_English(1, "modifier NH3").
modifier_in_English(2, "modifier HCOO").
modifier_in_English(3, "modifier CH3COO").
modifier_in_English(4, "modifier HCOONH3").
modifier_in_English(5, "modifier CH3COONH3").

% Maps an explanation list from the execution of adduct relation rules to a list of explanations in English.
map_explanation_list_to_English(_ThName, [], []).

map_explanation_list_to_English(ThName, [X | R], [E | Re]) :-
    explanation_in_English(ThName, adduct_relation_rule(X), E), 
    map_explanation_list_to_English(ThName, R, Re).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                          %
%   EXPLANATIONS FOR ADDUCT RELATION RULES                 %
%                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

has_identity_explained_in_English(N, EmpiricalCompound, Adduct, TheoreticalCompound, very_likely, adduct_relation_rule_explanation(core_information(CoreExplanationInEnglish))) :- 
    has_identity(N, EmpiricalCompound, TheoreticalCompound, ThName, Adduct, very_likely, adduct_relation_rule(E)),
    explanation_in_English(ThName, adduct_relation_rule(E), CoreExplanationInEnglish).

explanation_in_English(ThName, adduct_relation_rule([_TheoreticalCompound, e_compound(Mass1, RT1), Adduct1, e_compound(Mass2, RT2), Adduct2]), CoreExplanationInEnglish) :-
    concat(["It is very likely that the empirical compound ", Mass1, "@", RT1, " is the theoretical compound ", ThName, " with adduct ", Adduct1, ", because, for example, the empirical compound ", Mass2, "@", RT2, " with adduct ", Adduct2, " may be the theoretical compound ", ThName, " as well."], CoreExplanationInEnglish).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                          %
%   EXPLANATIONS FOR TIME RETENTION RULES                  %
%                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
has_identity_explained_in_English(N, EmpiricalCompound, Adduct, TheoreticalCompound, TrueValue, ExplanationInEnglish) :-
    has_identity(N, EmpiricalCompound, TheoreticalCompound, ThName, Adduct, TrueValue, retention_time_rule(E)),
    explanation_in_English(ThName, Adduct, retention_time_rule(E), ExplanationInEnglish).

% Explanation when there is only evidence for the rule.
% A good option with the current tests to check this clause is hypothesis 3.
explanation_in_English(ThName1, Adduct, retention_time_rule([e_compound(Mass1, RT1), _TheoreticalCompound1, LipidType, NCarbons1, NDoubleBonds1, [Xfor | _Rfor], []]), retention_time_rule(core_information(CoreExplanationInEnglish), evidences_for(Efor))) :-
    string_upper(LipidType, LipidTypeU),
    number_of_double_bonds_in_English(NDoubleBonds1, NDB1E),
    explanation_in_English_rt(Xfor, LipidType, Efor),
    %map_explanation_list_to_English_rt(LipidType, [Xfor | Rfor], Efor),
    concat(["It is very likely that the empirical compound ", Mass1, "@", RT1, " is the theoretical compound ", ThName1, " with adduct ", Adduct, ". The theoretical compound is a _", LipidTypeU, "_ lipid with ", NCarbons1, " carbons and ", NDB1E], CoreExplanationInEnglish).

% Explanation when there is only evidence both for and against the rule.
% A good option with the current tests to check this clause is hypothesis 496.
explanation_in_English(ThName1, Adduct, retention_time_rule([e_compound(Mass1, RT1), _TheoreticalCompound1, LipidType, NCarbons1, NDoubleBonds1, [Xfor | _Rfor], [Xagainst | _Ragainst]]), retention_time_rule(core_information(CoreExplanationInEnglish), evidences_for(Efor), evidences_against(Eagainst))) :-
    string_upper(LipidType, LipidTypeU),
    number_of_double_bonds_in_English(NDoubleBonds1, NDB1E),
    explanation_in_English_rt(Xfor, LipidType, Efor),
    %map_explanation_list_to_English_rt(LipidType, [Xfor | Rfor], Efor),
    explanation_in_English_rt(Xagainst, LipidType, Eagainst),
    %map_explanation_list_to_English_rt(LipidType, [Xagainst | Ragainst], Eagainst),
    concat(["It is likely that the empirical compound ", Mass1, "@", RT1, " is the theoretical compound ", ThName1, " with adduct ", Adduct, ". The theoretical compound is a _", LipidTypeU, "_ lipid with ", NCarbons1, " carbons and ", NDB1E], CoreExplanationInEnglish).

% Explanation when there is only evidence against the rule.
% A good option with the current tests to check this clause is hypothesis 205.
explanation_in_English(ThName1, Adduct, retention_time_rule([e_compound(Mass1, RT1), _TheoreticalCompound1, LipidType, NCarbons1, NDoubleBonds1, [], [Xagainst | _Ragainst]]), retention_time_rule(core_information(CoreExplanationInEnglish), evidences_against(Eagainst))) :-
    string_upper(LipidType, LipidTypeU),
    number_of_double_bonds_in_English(NDoubleBonds1, NDB1E),
    explanation_in_English_rt(Xagainst, LipidType, Eagainst),
    %map_explanation_list_to_English_rt(LipidType, [Xagainst | Ragainst], Eagainst),
    concat(["It is unlikely that the empirical compound ", Mass1, "@", RT1, " is the theoretical compound ", ThName1, " with adduct ", Adduct, ". The theoretical compound is a _", LipidTypeU, "_ lipid with ", NCarbons1, " carbons and ", NDB1E], CoreExplanationInEnglish).

% Maps an explanation list with its explanation in English for retention time rules.
map_explanation_list_to_English_rt(_LipidType, [], []).

% Case where a comma is required between evidences: '},{'. It is not the last evidence.
%map_explanation_list_to_English_rt(LipidType, [X | R], [E | Re]) :-
%    explanation_in_English_rt(X, LipidType, E),
%    map_explanation_list_to_English_rt(LipidType,  R, Re).

explanation_in_English_rt((N2, e_compound(Mass2, RT2), TheoreticalCompound2, NCarbons2, NDoubleBonds2), LipidType, ExplanationInEnglish) :-
    has_adduct(e_compound(Mass2, RT2), Adduct2, hypothesised, hypothesis(N2)),
    has_name(TheoreticalCompound2, ThName2, sure, given),
    string_upper(LipidType, LipidTypeU),
    number_of_double_bonds_in_English(NDoubleBonds2, NDB2E),
    concat(["For example, the compound ", Mass2, "@", RT2, " may be the theoretical compound ", ThName2, " with adduct ", Adduct2, ", another _", LipidTypeU, "_ lipid with ", NCarbons2, " carbons and ", NDB2E, "."], ExplanationInEnglish).

number_of_double_bonds_in_English(0, "no double bond").
number_of_double_bonds_in_English(1, "one double bond").
number_of_double_bonds_in_English(ND, English) :-
    ND \= 0, ND \=1,
    concat(ND, " double bonds", English).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                          %
%       AUXILIARY PREDICATES                               %
%                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

concat(L, E) :- concat_aux("", L, E).

concat_aux(A, [], A).
concat_aux(A, [X | R], E) :- concat(A, X, E1), concat_aux(E1, R, E). 
