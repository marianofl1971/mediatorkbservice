%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%             TESTS FOR AGGREGATION OF TRUE VALUES                 %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-consult('../true_values.pl').

test(aggregate_sure_test). 

test(aggregate_very_likely_test). 

test(aggregate_likely_test).

test(aggregate_some_unknown_test).

test(aggregate_hypothesised_test).

test(aggregate_some_very_unlikey_test).

test(aggregate_invented_true_value_test).

pass(aggregate_sure_test) :-
    aggregate_true_values_by_default([sure, sure, sure], sure).

pass(aggregate_very_likely_test) :-
    aggregate_true_values_by_default([sure, very_likely, sure], very_likely).

pass(aggregate_invented_true_value_test) :-
    \+ aggregate_true_values_by_default([invented, very_likely, sure], very_likely).

pass(aggregate_likely_test) :-
    aggregate_true_values_by_default([very_likely, likely, very_likely], likely).

pass(aggregate_hypothesised_test) :-
    aggregate_true_values_by_default([very_likely, hypothesised, likely], hypothesised).

pass(aggregate_some_unknown_test) :-
    aggregate_true_values_by_default([very_likely, likely, not_applicable], not_applicable).

pass(aggregate_some_very_unlikey_test) :-
    aggregate_true_values_by_default([very_likely, likely, very_unlikey], not_applicable).
