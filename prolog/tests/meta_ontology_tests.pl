%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                 TESTS FOR META-ONTOLOGY                          %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-consult('../meta_ontology.pl').

test(transitivity_class_hierarchy_test_1).
test(transitivity_class_hierarchy_test_2).

set_up :-
    dynamic(class/2),
    dynamic(is_directly_defined_instance_of/3),
    dynamic(is_directly_defined_sub_class_ofis_sub_class_of/3),
    assert(class(c1, sure, given)),
    assert(class(c2, sure, given)),
    assert(class(c3, sure, given)),
    assert(is_directly_defined_sub_class_of(c1, c2, sure, given)),
    assert(is_directly_defined_sub_class_of(c2, c3, sure, given)),
    assert(is_directly_defined_instance_of(i1, c1, sure, given)).

tear_down :-
    retractall(class(C1, TrueValue, Explanation)),
    retractall(is_directly_defined_sub_class_of(C1, C2, TrueValue, Explanation)),
    retractall(is_directly_defined_instance_of(C1, C2, TrueValue, Explanation)).

% If i1 is instance of c1, c1 is sub-class of c2 and c2 is sub-class-of c3
% => i1 is instance of c3.

pass(transitivity_class_hierarchy_test_1) :-
    set_up,
    is_instance_of(i1, c3, sure, inheritance),
    tear_down.

% If i1 is instance of c4, but c4 is not sub-class of c3 => i1 is not instance of c3

pass(transitivity_class_hierarchy_test_2) :-
    set_up,
    assert(class(c4, sure, given)),
    assert(is_directly_defined_instance_of(i2, c4, sure, given)),
    \+ is_instance_of(i2, c3, _TrueValue, _Explanation),
    tear_down.
