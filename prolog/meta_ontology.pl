%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                     META-ONTOLOGY                                %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-consult('true_values.pl').

is_instance_of(I, C, TrueValue, Explanation) :-
    is_directly_defined_instance_of(I, C, TrueValue, Explanation).

is_instance_of(I, C, TrueValue, Explanation) :-
    is_inferred_instance_of(I, C, TrueValue, Explanation).

is_inferred_instance_of(I, C2, ConclusionTrueValue, inheritance) :-
    class(C2, TrueValue1, _Explanation1),
    is_directly_defined_instance_of(I, C1, TrueValue2, _Explanation2),
    is_sub_class_of(C1, C2, TrueValue3, _Explanation3),
    aggregate_true_values_by_default([TrueValue1, TrueValue2, TrueValue3], ConclusionTrueValue).

is_sub_class_of(C1, C2, TrueValue, Explanation) :-
    is_directly_defined_sub_class_of(C1, C2, TrueValue, Explanation).

is_sub_class_of(C1, C2, TrueValue, Explanation) :-
    is_inferred_sub_class_of(C1, C2, TrueValue, Explanation).

is_inferred_sub_class_of(C, C, TrueValue, Explanation) :-
    class(C, TrueValue, Explanation).

is_inferred_sub_class_of(C1, C3, ConclusionTrueValue, inheritance) :-
    class(C1, TrueValue1, _Explanation1),
    is_directly_defined_sub_class_of(C1, C2, TrueValue2, _Explanation2),
    is_sub_class_of(C2, C3, TrueValue3, _Explanation3),
    C2 \= C3,
    aggregate_true_values_by_default([TrueValue1, TrueValue2, TrueValue3], ConclusionTrueValue).
