# Introduction

The main parts of the Prolog code are

1. The API REST, which listens at 8080 port.
2. The knowledge base.

Below are presented each one of these parts

# The API REST

If the user can run it in their own computer, the steps are

- Start SWI Prolog.

~~~~
swipl
~~~~
- Load the API REST

~~~~
?- ['api_rest_swipl.pl'].
~~~~
- Start the server

~~~~
?- server(8080).
~~~~

# The Prolog knowledge base

The Prolog knowledge base is structured as follows:

1. [The true value module](true_values.pl). It allows representing knowledge through a multi-valued
   logic. [Tests are provided for this module](tests/true_value_tests.pl). 
2. [The meta-ontology](meta_ontology.pl), which consults [the truev value module](true_values.pl). It implements frame
   oriented primitives: *instance of*, *sub-class of*, etc. using the logic
   values defined in *true_values.pl*. [The tests for this module](tests/meta_ontology_tests.pl) are also provided. 
3. [The compound ontology](compound_ontology.pl), which consults [the meta-ontology](meta_ontology.pl). It has been
   automatically generated from the Mediator database of metabolites. 
4. [The ionization rules](ionization_rules.pl), which consults no knowledge base. It has been
   automatically generated from a spreadsheet that contains the rules.
5. [The whole knowledge base](kb.pl), which consults [the ionization rules](ionization_rules.pl) and [the compound ontology](compound_ontology.pl).
   [Its tests](*tests/kb_tests.pl*) have been automatically generated from a
   spreadsheet with a colection of putative identifications of compounds.

More details of these modules can be found in the comments of their code.

